from dolfin import *

def ArtsActiveForce_Formulation_v2(w, w0, dw, wtest, f0, s0, n0, Tact, mesh, isvolctrl, num_field, dx1):

	if(isvolctrl):
		if(num_field == 1):
			(u,Pendo) = split(w);
			(u0,Pendo0) = split(w0);
			(Du,DPendo) = split(dw);
			(v,pendo) = split(wtest)

		elif(num_field == 2):
			(u,p,Pendo) = split(w);
			(u0,p0,Pendo0) = split(w0);
			(Du,Dp,DPendo) = split(dw);
			(v,q,pendo) = split(wtest)
		else:
			(u,p,Theta,Pendo) = split(w);
			(u0,p0,Theta0,Pendo0) = split(w0);
			(Du,Dp,DTheta,DPendo) = split(dw);
			(v,q,theta,pendo) = split(wtest)
	else:
		if(num_field == 1):
			u = w;
			u0 = w0;
			Du = dw;
			v = wtest

		elif(num_field == 2):
			(u,p) = split(w);
			(u0,p0) = split(w0);
			(Du,Dp) = split(dw);
			(v,q) = split(wtest)
		else:
			(u,p,Theta) = split(w);
			(u0,p0,Theta0) = split(w0);
			(Du,Dp,DTheta) = split(dw);
			(v,q,theta) = split(wtest)


	gradv_ff = inner(f0, grad(v)*f0)
	gradv_ss = inner(n0, grad(v)*n0)

	#Tmat = as_matrix([[Tact, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]])

	Lactive1 = inner(Tact, gradv_ff)*dx1

	return Lactive1
	#####################################################################################################################################


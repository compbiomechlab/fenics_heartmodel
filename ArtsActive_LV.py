from dolfin import *
import vtk as vtk
from math import *
import math as math
import os as os
import numpy as np
from edgetypebc import *
from addfiber import *
from matplotlib import pylab as plt
from edge import *
from ArtsActiveForce_Formulation import *
from GuccioneActiveForce_Formulation import *
import solver as solver
from inner_volume_constraint import *
from PassiveFungModel import *
import utilities as util
import csv
import updateplotutil as plotutil
import postprocess as postprocess



os.system("rm *.pvd")
os.system("rm *.vtu")

quad_degree = 4

dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3'
dolfin.parameters["form_compiler"]["cpp_optimize"] = True
dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["quadrature_degree"] = quad_degree
dolfin.parameters["form_compiler"]["representation"] = "uflacs"
dolfin.parameters['allow_extrapolation'] = True

class InitialConditionsPctrl(Expression):
    def eval(self, values, x):
        values[0] = 0.0 
        values[1] = 0.0 
        values[2] = 0.0 
        values[3] = 0.0 
        values[4] = 1.0
        values[5] = 0.0
        values[6] = 1.96
 
    def value_shape(self):
        return (7,)

class InitialConditionsVctrl(Expression):
    def eval(self, values, x):
        values[0] = 0.0 
        values[1] = 0.0 
        values[2] = 0.0 
        values[3] = 0.0 
        values[4] = 1.0
        values[5] = 0.0
        values[6] = 1.96
        values[7] = 0.0
 
    def value_shape(self):
        return (8,)


isvolctrl = False
endo_angle = 60
epi_angle = -60

# Geometry ------------------------------------------------------------------
topid = 2
endoid = 3
epiid = 1
casedir = "../mesh/ellipsoidal/"
casename = "../mesh/ellipsoidal/ellipsoidal_fc_coarse"
fibercasename = "../mesh/ellipsoidal/ellipsoidal_fc"

topid = 1
endoid = 3
epiid = 2
casedir = "../mesh/"
casename = "../mesh/ellipsoidal"
fibercasename = "../mesh/ellipsoidal"

meshfilename = casename + ".xml"
facetfilename = casename + "_facet_region.xml"
subdomainfilename = casename + "_physical_region.xml"
mesh = Mesh(meshfilename)
N = FacetNormal ( mesh )
facetboundaries = MeshFunction('size_t', mesh, facetfilename)

subdomains = MeshFunction("size_t", mesh, subdomainfilename)
rfun = MeshFunction("size_t", mesh, 1)
rfun.set_all(0)
bl = CompiledSubDomain("(x[1]*x[1] + x[0]*x[0]) > 3.5*3.5 - 1.0 && x[2]*x[2] < 0.1")
bl.mark(rfun, 10)

V = VectorFunctionSpace(mesh, "Lagrange", 2)
Q = FunctionSpace(mesh, "Lagrange",1)
DG = FunctionSpace(mesh, "DG",0)
Pspace = FunctionSpace(mesh, "Real", 0) 

if(dolfin.dolfin_version() == '1.7.0dev'):
	Quadelem = VectorElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, dim=3, quad_scheme='default')
	Quad = FunctionSpace(mesh, Quadelem)

	Quadscalar = FiniteElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme='default')
	Quads = FunctionSpace(mesh, Quadscalar)

else:
	Quad = VectorFunctionSpace(mesh, "Quadrature", quad_degree)
	Quads = FunctionSpace(mesh, "Quadrature", quad_degree)


if(isvolctrl):
	W = MixedFunctionSpace([V,DG,Q,Q,Q,Pspace])
else: 
	W = MixedFunctionSpace([V,DG,Q,Q,Q])

f0, s0, n0 = addfiber(mesh, Quad, casename, endo_angle, epi_angle, casedir)
#File('fiber.pvd') << f0
#File('sheet.pvd') << s0
#File('sheetnormal.pvd') << n0

l = Expression(("0.0", "0.0", "0.0"), degree = 1)
press = Expression(("P"), P = 0, degree = 1)
bctop = DirichletBC(W.sub(0).sub(2), Expression(("0.0"), degree = 1), facetboundaries, topid)
endoring = pick_endoring_bc()(rfun, 10)
bc_epi_edge = DirichletBC(W.sub(0), l, endoring, method = "pointwise")
bcs = [bc_epi_edge, bctop]
#bcs = [bctop]

dx = Measure("dx",domain=mesh)
ds = Measure("ds")[facetboundaries]  
##################################################################################################################################

## Define variational problem
w = Function(W)
w0 = Function(W)
w_prev = Function(W)
dw = TrialFunction(W)
wtest = TestFunction(W)

if(isvolctrl):
	(u,p,Theta,Carts,Larts,Pendo) = split(w);
	(u0,p0,Theta0,Carts0,Larts0,Pendo0) = split(w0);
	(Du,Dp,DTheta,DCarts,DLarts,DPendo) = split(dw);
	(v,q,theta,carts,larts,pendo) = split(wtest)

	w.interpolate(InitialConditionsVctrl())
	w_prev.interpolate(InitialConditionsVctrl())
	w0.interpolate(InitialConditionsVctrl())

else:
	(u,p,Theta,Carts,Larts) = split(w);
	(u0,p0,Theta0,Carts0,Larts0) = split(w0);
	(Du,Dp,DTheta,DCarts,DLarts) = split(dw);
	(v,q,theta,carts,larts) = split(wtest)

	w.interpolate(InitialConditionsPctrl())
	w_prev.interpolate(InitialConditionsPctrl())
	w0.interpolate(InitialConditionsPctrl())



d = u.geometric_dimension()
X = SpatialCoordinate(mesh)
I = Identity(d)             # Identity tensor
F = I + grad(u)             # Deformation gradient
F0 = I + grad(u0)        
Cmat = F.T*F       	    # Right Cauchy-Green tensor
E = Constant(0.5)*(Cmat - I)
Ic = variable(tr(Cmat))
J = variable(det(F))
J0 = det(F0)
Finv = inv(F)
n = cofac(F)*N
Press = Expression(("P"),P=0.0)

V_cav = util.cavityvol(w, X, n, ds, endoid)

V0 = Expression("vol", vol=V_cav, degree = 1)
t_a = Expression("value", value = 0.0, degree = 1)
dt = Expression("value", value = 1, degree = 1)
Tmax = Expression("value", value = 80000, degree = 1)

p_cav = Press.P
tstep = 0;
Kappa = Constant(1e9);
Kspr = Constant(1e3);

# Stored strain energy density (compressible neo-Hookean model)
Lpassive = PassiveFungModel(w, wtest, f0, s0, n0, mesh, isvolctrl, Kappa)
Lactive, Jactive = ArtsActiveForce_Formulation(w, w0, dw, wtest, f0, s0, n0, t_a, dt, Tmax, mesh, isvolctrl)
#Lactive, Jactive = GuccioneActiveForce_Formulation(w, w0, dw, wtest, f0, s0, n0, t_a, dt, Tmax, mesh, isvolctrl)
if(isvolctrl):
	print "Volume control"
	Lvol = inner_volume_constraint(w, w0, dw, wtest, V0, facetboundaries, endoid, mesh)
	L = Lpassive - Lvol #+ Kspr*inner(dot(u, n), dot(v, n))*ds(epiid)
else:
	print "Pressure control"
	L = Lpassive - dot(v, -Press*n)*ds(endoid) #+ Kspr*inner(dot(u, n), dot(v, n))*ds(epiid)

Ltotal = L + Lactive
Jac = derivative(Ltotal, w, TrialFunction(W))


dt.value = 2  # time step
BCL = 800.0
maxdt = 1
T = 1.0*BCL

t_niter = 40
r_threshold = 1e-7
w_prev.vector()[:] = w0.vector()

disp_vtk_file = File("nonlinearelasticity_disp.pvd")
p_vtk_file = File("nonlinearelasticity_p.pvd")
theta_vtk_file = File("nonlinearelasticity_theta.pvd")
Tact_vtk_file = File("nonlinearelasticity_Tact.pvd")
Ls_vtk_file = File("nonlinearelasticity_Ls.pvd")

LVcav_array = [V_cav]
Pcav_array = [p_cav]

fig = plt.figure()
ax = fig.add_subplot(111)
li, = ax.plot(LVcav_array, Pcav_array)
fig.canvas.draw()
plt.show(block=False)

Tmax.value = 0.001
for p in range(0, 10):
	if(isvolctrl):
		V0.vol += 5	
		print "Load step = ", p, " volume = ", V0.vol
	else:
		Press.P += 110.00
		print "Load step = ", p, " pressure = ", Press.P

	w, nIter, eps = solver.solveLVP(w, W, bcs, Jac, Ltotal, t_niter, r_threshold)
	V_cav = util.cavityvol(w, X, n, ds, endoid)

	Tact = postprocess.ActiveStressArts(w, Tmax, isvolctrl, f0, s0, n0)
	Tact = project(Tact, Q)
	Tact.rename("Tact", "Tact")
	Ls = postprocess.computeFiberStretch(w.split()[0], f0)
	Ls = project(Ls, Q)
	Ls.rename("Ls", "Ls")

	disp_vtk_file << (w.split()[0], float(p)*0.01)
	p_vtk_file << (w.split()[1], float(p)*0.01)
	theta_vtk_file << (w.split()[2], float(p)*0.01)
	Tact_vtk_file << (Tact, float(p)*0.01)
	Ls_vtk_file << (Ls, float(p)*0.01)



	LVcav_array.append(V_cav)
	if(isvolctrl):
		Pcav_array.append(util.cavitypressure(W,w)*0.0075)
	else:
		Pcav_array.append(Press.P*0.0075)


	plotutil.plotPV(li, fig, LVcav_array, Pcav_array)


Tmax.value = 80e3
#Tmax.value = 40e3
Cao = 10.0/1000.0;
Cven = 600.0/1000.0;
Vart0 = 500;#500;
Vven0 = 3200;#3200.0;
Rao = 1.5*1000.0 ;
Rven = 4.5*1000.0;
Rper = 140*1000.0;
V_ven = 3660 
V_art = 640


fdata = open('PV.txt', 'w')
while (tstep < T):
	isreset = False
	tstep += dt.value

	dt.value = 2.0

        cycle = floor(tstep/BCL)
	t_a.value = tstep - cycle*BCL
	print "Cycle number = ", cycle, " cell time = ", t_a.value, " tstep = ", tstep, " dt = ", dt.value, " Tmax.value = ", Tmax.value

	p_cav, w = util.computeNewPressure(w, W, X, n, ds, endoid, V_cav, Press.P, Jac, Ltotal, Press, bcs)
	V_cav = util.cavityvol(w, X, n, ds, endoid)

    	w0.vector()[:] = w.vector()

	Part = 1.0/Cao*(V_art - Vart0);
        Pven = 1.0/Cven*(V_ven - Vven0);
        PLV = p_cav;

        print "P_ven = ",Pven;
        print "P_LV = ", PLV;
        print "P_art = ", Part;

        if(PLV <= Part):
              Qao = 0.0;
        else:
             Qao = 1.0/Rao*(PLV - Part);
        

        if(PLV >= Pven):
            Qmv = 0.0;
        else: 
            Qmv = 1.0/Rven*(Pven - PLV);
        

        Qper = 1.0/Rper*(Part - Pven);

        print "Q_mv = ", Qmv ;
        print "Q_ao = ", Qao ;
        print "Q_per = ", Qper ;

	V_cav_prev = V_cav
	V_art_prev = V_art
	V_ven_prev = V_ven
	p_cav_prev = p_cav

        V_cav = V_cav + dt.value*(Qmv - Qao);
        V_art = V_art + dt.value*(Qao - Qper);
        V_ven = V_ven + dt.value*(Qper - Qmv);

        print "V_ven = ", V_ven;
        print "V_LV = ", V_cav;
        print "V_art = ", V_art;

	LVcav_array.append(V_cav)
	if(isvolctrl):
		Pcav_array.append(util.cavitypressure(W,w)*0.0075)
	else:
		Pcav_array.append(Press.P*0.0075)

	print >>fdata, t_a.value, V_cav, p_cav

	Tact = postprocess.ActiveStressArts(w, Tmax, isvolctrl, f0, s0, n0)
	Tact = project(Tact, Q)
	Tact.rename("Tact", "Tact")
	Ls = postprocess.computeFiberStretch(w.split()[0], f0)
	Ls = project(Ls, Q)
	Ls.rename("Ls", "Ls")


	disp_vtk_file << (w.split()[0], t_a.value)
	p_vtk_file << (w.split()[1], t_a.value)
	theta_vtk_file << (w.split()[2], t_a.value)
	Tact_vtk_file << (Tact, t_a.value)
	Ls_vtk_file << (Ls, t_a.value)

	plotutil.plotPV(li, fig, LVcav_array, Pcav_array)



if(isvolctrl):
	with open("PV_vctrl.txt", "w") as fdata:
		csv.writer(fdata, delimiter='\t').writerows(zip(Pcav_array, LVcav_array))
else:
	with open("PV_pctrl.txt", "w") as fdata:
		csv.writer(fdata, delimiter='\t').writerows(zip(Pcav_array, LVcav_array))

#Tmax.value = 100
#
#fdata = open("Tact.txt", "w")
#while (tstep < 200):
#	tstep += dt.value
#
#        cycle = floor(tstep/BCL)
#	t_a.time = tstep - cycle*BCL
#	print "cell time = ", t_a.time
#
#	################ DIAGNOSTIC ################################################################
#	F = I + grad(u)            
#	Cmat = F.T*F      	    
#	Cmatloc = (Qmat)*Cmat*(Qmat.T)
#	Tact = ArtsActiveForce(u, Carts, Larts, f0, Tmax)
#	print "Cmatloc[0,0] = ", project(Cmatloc[0,0],Q1)(5, 0.5, 0.5)
#	print "Carts = ", project(Carts,Q1)(5, 0.5, 0.5)
#	print "Larts = ", project(Larts,Q1)(5, 0.5, 0.5)
#	print "Ls = ", project(Cmatloc[0,0]*2.0,Q1)(5,0.5,0.5)
#	print "Ta = ", project(Tact,Q1)(5,0.5,0.5)
#	print >>fdata, t_a.time, project(Tact,Q1)(5,0.5,0.5), project(Carts,Q1)(5, 0.5, 0.5), project(Larts,Q1)(5, 0.5, 0.5)
#
#        Lbda_field = project(sqrt(Cmatloc[0,0]),Q1)
#	Lbda_field.rename("Lbda", "Lbda")
#	Tact_field = project(Tact, Q1)
#	Tact_field.rename("Tact", "Tact")
#
#	#disp_vtk_file << (w.split()[0], tstep)
#	#Lbda_vtk_file << (Lbda_field, tstep)
#	#P_vtk_file << (w.split()[1], tstep)
#	#Tact_vtk_file << (Tact_field, tstep)
#
#	################ DIAGNOSTIC ################################################################
#
#    	solver.solve(problem, w.vector())
#    	w0.vector()[:] = w.vector()
	
	


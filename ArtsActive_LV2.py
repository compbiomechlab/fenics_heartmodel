from dolfin import *
import numpy as np
import math as math
import postprocess as postprocess
from updateGuccioneVariable import *
from updateArtsVariable import *
import updateArtsVariable2 as Arts2
import utilities as util
from edgetypebc import *
from addfiber import *
from inner_volume_constraint import *
import sys

os.system("rm *.pvd")
os.system("rm *.vtu")


quad_degree = 4

def heaviside(x):
    return conditional(ge(x, 0.0), 1.0, 0.0)

# Optimization options for the form compiler
parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
ffc_options = {"optimize": True, \
               "precompute_basis_const": True, \
               "precompute_ip_const": True}
dolfin.parameters["form_compiler"]["quadrature_degree"] = quad_degree

isvolctrl = True
num_field = 1
endo_angle = 60
epi_angle = -60

# Geometry ------------------------------------------------------------------
#topid = 1
#endoid = 3
#epiid = 2
#casedir = "../mesh/"
#casename = "../mesh/ellipsoidal"
#fibercasename = "../mesh/ellipsoidal"
#meshfilename = casename + ".xml"
#facetfilename = casename + "_facet_region.xml"
#subdomainfilename = casename + "_physical_region.xml"
#mesh = Mesh(meshfilename)
#facetboundaries = MeshFunction('size_t', mesh, facetfilename)

#topid = 4
#endoid = 2
#epiid = 3
#mesh = Mesh("/media/lclee/DATAPART1/HenrikFenics/lvsolver/lv_mesh.xml")
#facetboundaries = MeshFunction("size_t", mesh, 2, mesh.domains())

topid = 2
endoid = 3
epiid = 1
casedir = "../mesh/ellipsoidal/"
#casename = "../mesh/ellipsoidal/ellipsoidal_fc"
casename = "../mesh/ellipsoidal/ellipsoidal_fc_coarse"
#fibercasename = "../mesh/ellipsoidal/ellipsoidal_fc"
fibercasename = "../mesh/ellipsoidal/ellipsoidal_fc_coarse"
meshfilename = casename + ".xml"
facetfilename = casename + "_facet_region.xml"
subdomainfilename = casename + "_physical_region.xml"
mesh = Mesh(meshfilename)
facetboundaries = MeshFunction('size_t', mesh, facetfilename)

topid = 4
endoid = 2
epiid = 3
casedir = "/home/likchuan/Dropbox/UKentuckyData/"
casename = "/home/likchuan/Dropbox/UKentuckyData/pmr120_baselinetri"
fibercasename = "/home/likchuan/Dropbox/UKentuckyData/pmr120_baselinetri"
meshfilename = casename + ".xml"
facetfilename = casename + "_facet_region.xml"
subdomainfilename = casename + "_physical_region.xml"
mesh = Mesh(meshfilename)
facetboundaries = MeshFunction('size_t', mesh, facetfilename)

#File("facetboundaries.pvd") << facetboundaries
#plot(facetboundaries, interactive=True)

N = FacetNormal ( mesh )
dx = dolfin.dx(mesh, metadata = {"integration_order":quad_degree})

subdomains = MeshFunction("size_t", mesh, subdomainfilename)
rfun = MeshFunction("size_t", mesh, 1)
rfun.set_all(0)
bl = CompiledSubDomain("(x[1]*x[1] + x[0]*x[0]) > 3.5*3.5 - 1.0 && x[2]*x[2] < 0.1")
bl.mark(rfun, 10)

ds = Measure("ds")[facetboundaries]  

V = VectorFunctionSpace(mesh, "P", 2)
P = FunctionSpace(mesh, "P", 1)
DG = FunctionSpace(mesh, "DG", 0)
VDG = VectorFunctionSpace(mesh, "DG", 0)
Pspace = FunctionSpace(mesh, "Real", 0) 

if(isvolctrl):
	W = MixedFunctionSpace([V,P,Pspace])
else:
	W = MixedFunctionSpace([V,P])

if(dolfin.dolfin_version() == '1.7.0dev'):

	Quadelem = VectorElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, dim=3, quad_scheme='default')
	Quad = FunctionSpace(mesh, Quadelem)

	Quadscalar = FiniteElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme='default')
	Quads = FunctionSpace(mesh, Quadscalar)
else:
	Quad = VectorFunctionSpace(mesh, "Quadrature", quad_degree)
	Quads = FunctionSpace(mesh, "Quadrature", quad_degree)

l = Expression(("0.0", "0.0", "0.0"), degree = 1)
press = Expression(("P"), P = 0, degree = 1)
if(isvolctrl):
	bctop = DirichletBC(W.sub(0).sub(2), Expression(("0.0"), degree = 1), facetboundaries, topid)
	#bctop = DirichletBC(W.sub(0).sub(0), Expression(("0.0"), degree = 1), facetboundaries, topid)
	endoring = pick_endoring_bc()(rfun, 10)
	bc_epi_edge = DirichletBC(W.sub(0), l, endoring, method = "pointwise")
else:
	bctop = DirichletBC(W.sub(0).sub(2), Expression(("0.0"), degree = 1), facetboundaries, topid)
	#bctop = DirichletBC(W.sub(0).sub(0), Expression(("0.0"), degree = 1), facetboundaries, topid)
	endoring = pick_endoring_bc()(rfun, 10)
	bc_epi_edge = DirichletBC(W.sub(0), l, endoring, method = "pointwise")

#bcs = [bc_epi_edge, bctop]
bcs = [bctop]

# Define functions
dw = TrialFunction(W)            # Incremental displacement
X = SpatialCoordinate(mesh)
wtest  = TestFunction(W)             # Test function
w  = Function(W)                 # Displacement from previous iteration
w0  = Function(W)                 # Displacement from previous iteration

Tact = Function(Quads)
Lart = Function(Quads)
Cart = Function(Quads)

#Tact = Function(P)
#Lart = Function(P)
#Cart = Function(P)

if(isvolctrl):
	(u,p,Pendo) = split(w);
	(u0,p0,Pendo0) = split(w0);
	(v,ptest,pendo) = split(wtest)
else:
	(u, p) = split(w)
	(u0, p0)= split(w0)
	(v, ptest) = split(wtest)

d = u.geometric_dimension()
I = Identity(d)             
F = I + grad(u)             
J = det(F)
Cmat = F.T*F       	    
Fdev = J**(-1.0/3.0)*F
Cdev = Fdev.T*Fdev
Kappa = Constant(1.0e5);
n = cofac(F)*N

f0, s0, n0 = addfiber(mesh, Quad, casename, endo_angle, epi_angle, casedir)
#V_f = VectorFunctionSpace(mesh, "Quadrature", 4)
#f0 = Function(V_f, "/media/lclee/DATAPART1/HenrikFenics/lvsolver/lv_fibers.xml")

#f0 = Expression(("0.0","0.0","1.0"))
#s0 = Expression(("0.0","1.0","0.0"))
#n0 = Expression(("1.0","0.0","0.0"))

Press = Expression(("P"), P=0.0)
V_cav = util.cavityvol(u, X, n, ds, endoid, num_field)
V0 = Expression("vol", vol=V_cav, degree = 1)


#Ccoeff = Constant(115*5);
#bf = Constant(14.4);
#bfx = Constant(0.7*14.4);
#bxx = Constant(0.4*14.4);
Ccoeff = Constant(876);
bf = Constant(18.48);
bfx = Constant(1.627);
bxx = Constant(3.58);
Kspring = Constant(10.00);
Edev = Constant(0.5)*(Cdev - I)
I4f = inner(f0, Cdev*f0)
I1 = tr(Cdev)
#Qmat = util.Qtransform(f0, s0, n0)
#Edevloc = (Qmat)*Edev*(Qmat.T)

#QQ = bf*Edevloc[0,0]**2 + bxx*(Edevloc[1,1]**2 + Edevloc[2,2]**2 + Edevloc[1,2]**2 + Edevloc[2,1]**2) + bfx*(Edevloc[0,1]**2 + Edevloc[1,0]**2 + Edevloc[0,2]**2 + Edevloc[0,2]**2)
#WFung = Ccoeff*(exp(QQ) - 3) + p*(J - 1.0)
	
a = 496
a_f = 3283
b = 7.209
b_f = 11.176
W1Hol = a/(2.0*b) * (exp(b*(I1 - 3)) - 1)
W2Hol = a_f/(2.0*b_f) * heaviside(I4f - 1) * (exp(b_f*pow(I4f - 1, 2)) - 1)
WHol = W1Hol + W2Hol + p*(J - 1.0) #Kappa/Constant(2.0)*(J - 1.0)**2.0

Wactive = Tact * (I4f)
Wtotal = WHol #+ Wactive
#Wtotal = WFung + Wactive

# Total potential energy
if(isvolctrl):
	Pi = Wtotal*dx + inner_volume_constraint(w, w0, dw, wtest, V0, facetboundaries, endoid, mesh)
else:
	Pi = Wtotal*dx  + inner(u, 1.0*Press*n)*ds(endoid)

# Compute first variation of Pi (directional derivative about u in the direction of v)
F = derivative(Pi, w, wtest) + inner(Tact, inner(f0, grad(v)*f0))*dx  - Kspring*inner(u,v)*ds(epiid)

# Compute Jacobian of F
Jac = derivative(F, w, dw)

t_niter = 40
r_threshold = 1e-9

# Solve variational problem
dt = 2.0
t = 0.0
Tmax = 80e3
Tend = 800.0;
ngpt = len(Tact.vector().array()[:])
t_niter = 40
r_threshold = 1e-9

p_cav = Press.P

# Varying the inhomogenity of the stiffness
disp_vtk_file = File("nonlinearelasticity_block_disp_diag.pvd")
Tact_vtk_file = File("nonlinearelasticity_block_Tact_diag.pvd")
Lbda_vtk_file = File("nonlinearelasticity_block_lbda_diag.pvd")
Tmax = 0.0001

fdataPV = open("PV.txt", "w", 0)
p_cav = 0.0
v_cav = V_cav
LVcav_array = [V_cav]
Pcav_array = [p_cav]
print >>fdataPV, p_cav, V_cav
disp_vtk_file << (w.split()[0], 0.0)

for pp in range(0, 10):
	p_cav += 100.00
	v_cav += 3.00

	print "Load step = ", pp
	V_cav, p_cav = util.computeNewVolume(p_cav, v_cav, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W)
	disp_vtk_file << (w.split()[0], pp*0.01)

	LVcav_array.append(V_cav)
	Pcav_array.append(p_cav)

	print >>fdataPV, p_cav, V_cav

Tmax = 80e3
BCL = 800.0
tstep = 0
Cao = 10.0/1000.0;
Cven = 600.0/1000.0;
Vart0 = 500;#500;
Vven0 = 3200;#3200.0;
Rao = 1.5*1000.0 ;
Rven = 4.5*1000.0;
Rper = 140*1000.0;
V_ven = 3660 
V_art = 640

fdata = open("Tact.txt","w")

while(t < 1000):#Tend):


	# Modifying stiffness at the Gauss points
	Cart_gpt = Cart.vector().array()[:]
	Lart_gpt = Lart.vector().array()[:]
	Tact_gpt = Tact.vector().array()[:]

	lbda = postprocess.computeFiberStretch(u, f0)
	lbda_gpt = project(lbda, Quads).vector().array()[:]
	#lbda_gpt = project(lbda, P).vector().array()[:]

	Cart_gpt, Lart_gpt, Tact_gpt = updateGuccioneVariable(Tmax, Lart_gpt, Cart_gpt, dt, lbda_gpt, t)
	#Cart_gpt, Lart_gpt, Tact_gpt = updateArtsVariable(Tmax, Lart_gpt, Cart_gpt, dt, lbda_gpt, t)
	#Cart_gpt, Lart_gpt, Tact_gpt = Arts2.updateArtsVariable_v2(Tmax, Lart_gpt, Cart_gpt, dt, lbda_gpt, t)
	print >>fdata, t, Tact_gpt[192], lbda_gpt[192], Cart_gpt[192], Lart_gpt[192], Tact_gpt[192]	

	print "time = ", t
	dt = 0.50
	tstep = tstep + dt

        cycle = math.floor(tstep/BCL)
	t = tstep - cycle*BCL
	print "Cycle number = ", cycle, " cell time = ", t, " tstep = ", tstep, " dt = ", dt


	print "Max stiffness = ", max(Tact.vector().array()),  " Min stiffness = ", min(Tact.vector().array())
	print lbda_gpt[192], min(lbda_gpt), max(lbda_gpt), np.argmin(lbda_gpt)
	print Cart_gpt[192], min(Cart_gpt), max(Cart_gpt), np.argmin(Cart_gpt)
	print Lart_gpt[192], min(Lart_gpt), max(Lart_gpt), np.argmin(Lart_gpt)
	print Tact_gpt[192], min(Tact_gpt), max(Tact_gpt), np.argmin(Tact_gpt)


	Cart.vector()[:] = Cart_gpt
	Lart.vector()[:] = Lart_gpt
	Tact.vector()[:] = Tact_gpt

	Tact_elms = project(Tact, P)
	Tact_elms.rename("Tact", "Tact")
	Lbda_elms = project(lbda, P)
	Lbda_elms.rename("lbda", "lbda")

	#solve(F == 0, u, bcs, J=Jac,form_compiler_parameters=ffc_options)
	if(isvolctrl):
		V_cav, p_cav = util.computeNewVolume(p_cav, v_cav, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W)
	else:
		p_cav, w = util.computeNewPressure(V_cav, p_cav, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W)
		V_cav = util.cavityvol(w.split()[0], X, n, ds, endoid, num_field)


	print >>fdataPV, p_cav, V_cav

	Part = 1.0/Cao*(V_art - Vart0);
        Pven = 1.0/Cven*(V_ven - Vven0);
        PLV = p_cav;

	print "P_ven = ",Pven;
        print "P_LV = ", PLV;
        print "P_art = ", Part;

        if(PLV <= Part):
             Qao = 0.0;
        else:
             Qao = 1.0/Rao*(PLV - Part);
        

        if(PLV >= Pven):
            Qmv = 0.0;
        else: 
            Qmv = 1.0/Rven*(Pven - PLV);
        

        Qper = 1.0/Rper*(Part - Pven);

        print "Q_mv = ", Qmv ;
        print "Q_ao = ", Qao ;
        print "Q_per = ", Qper ;

	V_cav_prev = V_cav
	V_art_prev = V_art
	V_ven_prev = V_ven
	p_cav_prev = p_cav

        V_cav = V_cav + dt*(Qmv - Qao);
        V_art = V_art + dt*(Qao - Qper);
        V_ven = V_ven + dt*(Qper - Qmv);

        print "V_ven = ", V_ven;
        print "V_LV = ", V_cav;
        print "V_art = ", V_art;

	disp_vtk_file << (w.split()[0], tstep)
	Tact_vtk_file << (Tact_elms, tstep)
	Lbda_vtk_file << (Lbda_elms, tstep)

	v_cav = V_cav



#
## Plot and hold solution
#plot(u, interactive=True, mode="displacement")


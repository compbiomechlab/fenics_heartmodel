from dolfin import *
import numpy as np
import math as math
import postprocess as postprocess
import utilities as util
from edgetypebc import *
from addfiber import *
from inner_volume_constraint import *
import sys

os.system("rm *.pvd")
os.system("rm *.vtu")


quad_degree = 4

# Optimization options for the form compiler
parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
ffc_options = {"optimize": True, \
               "precompute_basis_const": True, \
               "precompute_ip_const": True}
dolfin.parameters["form_compiler"]["quadrature_degree"] = quad_degree

isvolctrl = True
num_field = 1
endo_angle = 60
epi_angle = -60

# Geometry ------------------------------------------------------------------
topid = 1
endoid = 3
epiid = 2
casedir = "./mesh/"
casename = "./mesh/ellipsoidal"
fibercasename = "./mesh/ellipsoidal"
meshfilename = casename + ".xml"
facetfilename = casename + "_facet_region.xml"
subdomainfilename = casename + "_physical_region.xml"
mesh = Mesh(meshfilename)
facetboundaries = MeshFunction('size_t', mesh, facetfilename)

N = FacetNormal ( mesh )
dx = dolfin.dx(mesh, metadata = {"integration_order":quad_degree})

subdomains = MeshFunction("size_t", mesh, subdomainfilename)
rfun = MeshFunction("size_t", mesh, 1)
rfun.set_all(0)
bl = CompiledSubDomain("(x[1]*x[1] + x[0]*x[0]) > 3.5*3.5 - 1.0 && x[2]*x[2] < 0.1")
bl.mark(rfun, 10)

ds = Measure("ds")[facetboundaries]  


if(dolfin.dolfin_version() != '1.6.0'):
	Velem = VectorElement("Lagrange", mesh.ufl_cell(), 2)
	Pelem = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
	DGelem = FiniteElement("DG", mesh.ufl_cell(), 0)
	VDGelem = VectorElement("DG", mesh.ufl_cell(), 0)
	Pspace_elem = FiniteElement("Real", mesh.ufl_cell(), 0)
	
	V = FunctionSpace(mesh, Velem)
	P = FunctionSpace(mesh, Pelem)
	DG = FunctionSpace(mesh, DGelem)
	VDG = FunctionSpace(mesh, VDGelem)
	Pspace = FunctionSpace(mesh, Pspace_elem)

	if(isvolctrl):
		W = FunctionSpace(mesh, MixedElement([Velem,Pelem,Pspace_elem]))
	else:
		W = FunctionSpace(mesh, MixedElement([Velem,Pelem]))

else:

	V = VectorFunctionSpace(mesh, "P", 2)
	P = FunctionSpace(mesh, "P", 1)
	DG = FunctionSpace(mesh, "P", 1)
	VDG = VectorFunctionSpace(mesh, "DG", 0)
	Pspace = FunctionSpace(mesh, "Real", 0) 


	if(isvolctrl):
		W = MixedFunctionSpace([V,P,Pspace])
	else:
		W = MixedFunctionSpace([V,P])



if(dolfin.dolfin_version() != '1.6.0'):

	Quadelem = VectorElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, dim=3, quad_scheme='default')
	Quad = FunctionSpace(mesh, Quadelem)

	Quadscalar = FiniteElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme='default')
	Quads = FunctionSpace(mesh, Quadscalar)
else:
	Quad = VectorFunctionSpace(mesh, "Quadrature", quad_degree)
	Quads = FunctionSpace(mesh, "Quadrature", quad_degree)

l = Expression(("0.0", "0.0", "0.0"), degree = 2)
press = Expression(("P"), P = 0, degree = 2)

# Add B.C.
if(isvolctrl):
	bctop = DirichletBC(W.sub(0).sub(2), Expression(("0.0"), degree = 1), facetboundaries, topid)
	endoring = pick_endoring_bc()(rfun, 10)
else:
	bctop = DirichletBC(W.sub(0).sub(2), Expression(("0.0"), degree = 1), facetboundaries, topid)
	endoring = pick_endoring_bc()(rfun, 10)

bcs = [bctop]

# Define functions
dw = TrialFunction(W)            # Incremental displacement
X = SpatialCoordinate(mesh)
wtest  = TestFunction(W)             # Test function
w  = Function(W)                 # Displacement from previous iteration
w0  = Function(W)                 # Displacement from previous iteration

Tact = Function(DG)
Lart = Function(DG)
Lart.vector()[:] = (1.85 - 0.04)*np.ones(len(Lart.vector().array()[:])) 
Cart = Function(DG)

if(isvolctrl):

	(u,p,Pendo) = split(w);
	(u0,p0,Pendo0) = split(w0);
	(v,ptest,pendo) = split(wtest);

	#(u,p,Pendo) = split(w);
	#(u0,p0,Pendo0) = split(w0);
	#(v,ptest,pendo) = split(wtest)

	#(u,Pendo) = split(w);
	#(u0,Pendo0) = split(w0);
	#(v,pendo) = split(wtest)

else:
	(u, p) = split(w)
	(u0, p0)= split(w0)
	(v, ptest) = split(wtest)

d = u.geometric_dimension()
I = Identity(d)             
F = I + grad(u)             
J = det(F)
Cmat = F.T*F       	    
Fdev = pow(J, -1.0/3.0)*F
Cdev = pow(J, -2.0/3.0)*Cmat
Kappa = Constant(1.0e6);
n = cofac(F)*N

f0, s0, n0 = addfiber(mesh, Quad, casename, endo_angle, epi_angle, casedir)

Press = Expression(("P"), P=0.0, degree = 1)
V_cav = util.cavityvol(u, X, n, ds, endoid, num_field)
V0 = Expression("vol", vol=V_cav, degree = 1)
tt = Expression("t", t=0, degree = 1)

Ccoeff = Constant(400);
bf = Constant(18.48);
bfx = Constant(1.627);
bxx = Constant(3.58);
Kspring = Constant(1.00);
Edev = Constant(0.5)*(Cdev - I)
Eff = inner(f0, Edev*f0)
Ess = inner(s0, Edev*s0)
Enn = inner(n0, Edev*n0)
Efs = inner(f0, Edev*s0)
Efn = inner(f0, Edev*n0)
Ens = inner(n0, Edev*s0)

I4f = inner(f0, Cmat*f0)
I1 = tr(Cdev)
M1ij = f0[i]*f0[j]

QQ = bf*pow(Eff,2.0) + bxx*(pow(Ess,2.0)+ pow(Enn,2.0)+ 2.0*pow(Ens,2.0)) + bfx*(2.0*pow(Efs,2.0) + 2.0*pow(Efn,2.0))
WFung = Ccoeff*(exp(QQ) - 1.0) + p*(J - 1.0)
	
Tmax = 250e3

Wtotal = WFung
Lsref = Constant(1.85)
Lseiso = Constant(0.04)
td = Constant(150)
tmax = Constant(600)
tr0 = Constant(100)
a4 = Constant(10.0)
a6 = Constant(2.0)
a7 = Constant(1.51)
Ft = pow(tanh(tt/tr0),2.0)*pow(tanh((tmax - tt)/td),2.0)*conditional(lt(tt,tmax),1.0,0.0)
S_active = Tmax*tanh(a6*(sqrt(I4f)*Lsref - a7))*Ft
sigma_act = F*S_active*as_tensor(M1ij, (i,j)) 

# Total potential energy
if(isvolctrl):
	Pi = Wtotal*dx + inner_volume_constraint(w, w0, dw, wtest, V0, facetboundaries, endoid, mesh) - Kspring*pow(inner(u, n), 2.0)*ds(epiid)
else:
	Pi = Wtotal*dx  + inner(u, 1.0*Press*n)*ds(endoid)  - inner(Kspring*u, v)*ds(epiid)

# Compute first variation of Pi (directional derivative about u in the direction of v)
F = derivative(Pi, w, wtest) + inner(sigma_act, grad(v))*dx 

# Compute Jacobian of F
Jac = derivative(F, w, dw)

t_niter = 40
r_threshold = 1e-9

# Solve variational problem
dt = 1.0
t = 0.0
Tend = 800.0;
ngpt = len(Tact.vector().array()[:])
t_niter = 40
r_threshold = 1e-9

p_cav = Press.P

# Varying the inhomogenity of the stiffness
disp_vtk_file = File("nonlinearelasticity_block_disp_diag.pvd")

fdataPV = open("PV.txt", "w", 0)
p_cav = 0.0
v_cav = V_cav
LVcav_array = [V_cav]
Pcav_array = [p_cav]
print >>fdataPV, t, p_cav, V_cav
disp_vtk_file << (w.split()[0], 0.0)

for pp in range(0, 10):
	p_cav += 100.00
	v_cav += 2.00

	print "Load step = ", pp
	V_cav, p_cav = util.computeNewVolume(p_cav, v_cav, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W)
	disp_vtk_file << (w.split()[0], pp*0.01)

	LVcav_array.append(V_cav)
	Pcav_array.append(p_cav)

	print >>fdataPV, pp*0.01, p_cav, V_cav

BCL = 800.0
tstep = 0
Cao = 10.0/1000.0;
Cven = 400.0/1000.0;
Vart0 = 510;#500;
Vven0 = 2800;#3200.0;
Rao = 5*1000.0 ;
Rven = 2.0*1000.0;
Rper = 100*1000.0;
V_ven = 3660 
V_art = 640

fdata = open("Tact.txt","w")

cycle = 0.0
outputgpt = 2
while(cycle < 3):#Tend):

	if(t >= 0 and t < 100):
		dt = 0.25
	elif(t >= 100 and t < 150):
		dt = 0.5
	elif(t >= 150 and t < 450):
		dt = 1.0
	elif(t >= 550 and t < 600):
		dt = 0.25
	else:
		dt = 1.0

	print "time = ", t
	#dt = 0.50
	tstep = tstep + dt

        cycle = math.floor(tstep/BCL)
	t = tstep - cycle*BCL

	tt.t = t
		
	print "Cycle number = ", cycle, " cell time = ", t, " tstep = ", tstep, " dt = ", dt

	disp_vtk_file << (w.split()[0], tstep)


	if(isvolctrl):
		V_cav, p_cav = util.computeNewVolume(p_cav, v_cav, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W)
	else:
		p_cav, w = util.computeNewPressure(V_cav, p_cav, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W)
		V_cav = util.cavityvol(w.split()[0], X, n, ds, endoid, num_field)


	print >>fdataPV, tstep, p_cav, V_cav

	Part = 1.0/Cao*(V_art - Vart0);
        Pven = 1.0/Cven*(V_ven - Vven0);
        PLV = p_cav;

	print "P_ven = ",Pven;
        print "P_LV = ", PLV;
        print "P_art = ", Part;

        if(PLV <= Part):
             Qao = 0.0;
        else:
             Qao = 1.0/Rao*(PLV - Part);
        

        if(PLV >= Pven):
            Qmv = 0.0;
        else: 
            Qmv = 1.0/Rven*(Pven - PLV);
        

        Qper = 1.0/Rper*(Part - Pven);

        print "Q_mv = ", Qmv ;
        print "Q_ao = ", Qao ;
        print "Q_per = ", Qper ;

	V_cav_prev = V_cav
	V_art_prev = V_art
	V_ven_prev = V_ven
	p_cav_prev = p_cav


        V_cav = V_cav + dt*(Qmv - Qao);
        V_art = V_art + dt*(Qao - Qper);
        V_ven = V_ven + dt*(Qper - Qmv);

        print "V_ven = ", V_ven;
        print "V_LV = ", V_cav;
        print "V_art = ", V_art;

	v_cav = V_cav






from dolfin import *
from math import *
import math as math
import os as os
import numpy as np
from edgetypebc import *
from addfiber import *
from matplotlib import pylab as plt
from edge import *
from ArtsActiveForce_Formulation_v2 import *
import solver as solver
from PassiveFungModel import *
import postprocess as postprocess
from updateArtsVariable import *
from updateGuccioneVariable import *
from vtk_py import *

def writenodalresults(w, mesh, V, t_a):

	gdim = mesh.geometry().dim()
	xdofmap = V.sub(0).dofmap().dofs()
	ydofmap = V.sub(1).dofmap().dofs()
	zdofmap = V.sub(2).dofmap().dofs()


	if(dolfin.dolfin_version() == '1.7.0dev'):
		xq = V.tabulate_dof_coordinates().reshape((-1, gdim))
		xq0 = xq[xdofmap]  
	else:
		xq = V.dofmap().tabulate_all_coordinates(mesh).reshape((-1, gdim))
		xq0 = xq[xdofmap]  

	# Create an unstructured grid of Gauss Points
	points = vtk.vtkPoints()
	ugrid = vtk.vtkUnstructuredGrid()
	cnt = 0;
	for pt in xq0:
		points.InsertNextPoint(pt)
		cnt += 1

	ugrid.SetPoints(points)

	CreateVertexFromPoint(ugrid)

	disp_vector =  vtk.vtkFloatArray()
	disp_vector.SetName("disp")
	disp_vector.SetNumberOfComponents(3)

	#print w.vector().array()[:]
	cnt = 0;
	for pt in xq0:

		disp_vector.InsertNextTuple3(w.vector()[xdofmap[cnt]], w.vector()[ydofmap[cnt]], w.vector()[zdofmap[cnt]])
		cnt += 1

	ugrid.GetCellData().SetVectors(disp_vector)
	filename = "disp_pt" + str(int(t_a)) + ".vtu"
	#writeXMLUGrid(ugrid, filename)




os.system("rm nonlinearelasticity_block*")
os.system("rm disp_pt*")

quad_degree = 8

dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3'
dolfin.parameters["form_compiler"]["cpp_optimize"] = True
dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["quadrature_degree"] = quad_degree
dolfin.parameters["form_compiler"]["representation"] = "uflacs"
dolfin.parameters['allow_extrapolation'] = True

class InitialConditions3Field(Expression):
    def eval(self, values, x):
        values[0] = 0.0 
        values[1] = 0.0 
        values[2] = 0.0 
        values[3] = 0.0 
        values[4] = 1.0
         
    def value_shape(self):
        return (5,)

class InitialConditions2Field(Expression):
    def eval(self, values, x):
        values[0] = 0.0 
        values[1] = 0.0 
        values[2] = 0.0 
        values[3] = 0.0 
         
    def value_shape(self):
        return (4,)


class InitialConditions1Field(Expression):
    def eval(self, values, x):
        values[0] = 0.0 
        values[1] = 0.0 
        values[2] = 0.0 
         
    def value_shape(self):
        return (3,)


t_a = 0.0
dt = 1.0
Tmax = 80e3
tstep = 0;

isvolctrl = False 
num_field = 1
# Geometry ------------------------------------------------------------------
L = 10.0
mesh = BoxMesh(Point(0.0, 0.0, 0.0), Point(L, 1.0, 1.0), 10, 1, 1)
N = FacetNormal ( mesh )

# Mark boundary subdomians
left =  CompiledSubDomain("near(x[0], side) && on_boundary", side = 0.0)
right = CompiledSubDomain("near(x[0], side) && on_boundary", side = L)
top = CompiledSubDomain("near(x[2], side) && on_boundary", side = 1.0)
bot = CompiledSubDomain("near(x[2], side) && on_boundary", side = 0.0)
front = CompiledSubDomain("near(x[1], side) && on_boundary", side = 1.0)
back = CompiledSubDomain("near(x[1], side) && on_boundary", side = 0.0)

# Define essential boundary
facetboundaries = FacetFunction("size_t", mesh)     
facetboundaries.set_all(0) 
right.mark(facetboundaries, 2)
left.mark(facetboundaries, 1)
top.mark(facetboundaries, 3)
front.mark(facetboundaries, 4)
bot.mark(facetboundaries, 5)
back.mark(facetboundaries, 6)

V = VectorFunctionSpace(mesh, "Lagrange", 2)
Q = FunctionSpace(mesh, "Lagrange",1)
DG = FunctionSpace(mesh, "DG",0)

if(num_field == 1):
	W = V
elif(num_field == 2):
	W = MixedFunctionSpace([V,Q])
else:
	W = MixedFunctionSpace([V,Q,Q])

if(dolfin.dolfin_version() == '1.7.0dev'):
	#QuadTelem = TensorElement(family="Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme='default', shape=(3,3))
	#QuadT = FunctionSpace(mesh, QuadTelem)

	Quadelem = VectorElement(family="Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, dim=3, quad_scheme='default')
	Quad = FunctionSpace(mesh, Quadelem)

	Quadscalar = FiniteElement(family="Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme='default')
	Quads = FunctionSpace(mesh, Quadscalar)

else:
	Quad = VectorFunctionSpace(mesh, "Quadrature", quad_degree)
	Quads = FunctionSpace(mesh, "Quadrature", quad_degree)
	#QuadT = TensorFunctionSpace(mesh, "Quadrature", quad_degree)



f0 = Constant(("1.0", "0.0", "0.0"))
s0 = Constant(("0.0", "1.0", "0.0"))
n0 = Constant(("0.0", "0.0", "1.0"))

dispExp = Expression(("u"), degree = 2, u = 0.0)

if(num_field > 1):
	bcright = DirichletBC(W.sub(0).sub(0), dispExp, facetboundaries, 2)
	bctop = DirichletBC(W.sub(0).sub(2), Constant(("0")), facetboundaries, 3)
	bcfront = DirichletBC(W.sub(0).sub(1), Constant(("0")), facetboundaries, 4)
	bcleft = DirichletBC(W.sub(0).sub(0), Constant(("0")), facetboundaries, 1)

else:
	bcright = DirichletBC(W.sub(0), dispExp, facetboundaries, 2)
	bctop = DirichletBC(W.sub(2), Constant(("0")), facetboundaries, 3)
	bcfront = DirichletBC(W.sub(1), Constant(("0")), facetboundaries, 4)
	bcleft = DirichletBC(W.sub(0), Constant(("0")), facetboundaries, 1)


bcs1 = [bcleft, bctop, bcfront]
bcs2 = [bcleft, bctop, bcfront, bcright]#, bcbot, bcback]

dx1 = dolfin.dx(mesh, metadata = {"integration_order":quad_degree})
ds = Measure("ds")[facetboundaries]  
##################################################################################################################################

## Define variational problem
w = Function(W)
w0 = Function(W)
dw = TrialFunction(W)
wtest = TestFunction(W)

if(num_field == 1):
	u = w;
	u0 = w0;
	Du = dw;
	v = wtest

	u.interpolate(InitialConditions1Field(degree=1))
	u0.interpolate(InitialConditions1Field(degree=1))


elif(num_field == 2):
	(u,p) = split(w);
	(u0,p0) = split(w0);
	(Du,Dp) = split(dw);
	(v,q) = split(wtest)

	w.interpolate(InitialConditions2Field(degree=1))
	w0.interpolate(InitialConditions2Field(degree=1))


else:
	(u,p,Theta) = split(w);
	(u0,p0,Theta0) = split(w0);
	(Du,Dp,DTheta) = split(dw);
	(v,q,theta) = split(wtest)

	w.interpolate(InitialConditions3Field(degree=1))
	w0.interpolate(InitialConditions3Field(degree=1))

d = u.geometric_dimension()
X = SpatialCoordinate(mesh)
I = Identity(d)             # Identity tensor
F = I + grad(u)             # Deformation gradient
F0 = I + grad(u0)        
Cmat = F.T*F       	    # Right Cauchy-Green tensor
E = Constant(0.5)*(Cmat - I)
Ic = variable(tr(Cmat))
J = variable(det(F))
J0 = det(F0)
Finv = inv(F)
n = J*(Finv.T)*N
Press = Expression(("P"),P=0.0)
Kappa = Constant(1.0e9);

Tact = Function(Quads)
Lart = Function(Quads)
Cart = Function(Quads)

#print len(Tact.vector().array()[:])

Lpassive = PassiveFungModel(w, wtest, f0, s0, n0, mesh, isvolctrl, num_field, Kappa, dx1, Tact)
Lactive = ArtsActiveForce_Formulation_v2(w, w0, dw, wtest, f0, s0, n0, Tact, mesh, isvolctrl, num_field, dx1)
L = Lpassive #- dot(v, Press*n)*ds(2)
Ltotal = L #+ Lactive

Jac = derivative(Ltotal, w, TrialFunction(W))

dt = 1.0  # time step
BCL = 800.0
maxdt = 1
Tend = BCL

t_niter = 40
r_threshold = 1e-9

disp_vtk_file = File("nonlinearelasticity_block_disp_diag.pvd")
p_vtk_file = File("nonlinearelasticity_block_p_diag.pvd")
theta_vtk_file = File("nonlinearelasticity_block_theta_diag.pvd")


if(num_field == 1):
	disp_vtk_file << (w, 0.00)

if(num_field > 1):
	p_vtk_file << (w.split()[1], 0.00)
	disp_vtk_file << (w.split()[0], 0.00)
if(num_field > 2):
	theta_vtk_file << (w.split()[2], 0.00)

#Tmax = 0.0001
#for p in range(0, 5):
#	dispExp.u += 0.0
#	Press.P += 500.00
#
#	print "Load step = ", p, "disp = ", dispExp.u
#	w, nIter, eps = solver.solveLVP(w, W, bcs2, Jac, Ltotal, t_niter, r_threshold)
#
#
#	if(num_field > 1):
#		disp_vtk_file << (w.split()[0], float(p+1)*0.01)
#		p_vtk_file << (w.split()[1], float(p+1)*0.01)
#	if(num_field > 2):
#		disp_vtk_file << (w.split()[0], float(p+1)*0.01)
#		theta_vtk_file << (w.split()[2], float(p+1)*0.01)
#	if(num_field == 1):
#		disp_vtk_file << (w, float(p+1)*0.01)
#
Tmax = 80e3
fdata = open("Tact.txt","w")
while (tstep < Tend):
	tstep += dt
	Press.P += 0.0

	dt = 1.0

        cycle = floor(tstep/BCL)
	t_a = tstep - cycle*BCL

	print "Cycle number = ", cycle, " cell time = ", t_a, " tstep = ", tstep, " dt = ", dt, " Tmax = ", Tmax

	Cart_gpt = Cart.vector().array()[:]
	Lart_gpt = Lart.vector().array()[:]
	Tact_gpt = Tact.vector().array()[:]

	if(num_field > 1):
		lbda = postprocess.computeFiberStretch(w.split()[0], f0)
	else:
		lbda = postprocess.computeFiberStretch(w, f0)
	lbda_gpt = project(lbda, Quads).vector().array()[:]
	#Cart_gpt, Lart_gpt, Tact_gpt = updateArtsVariable(Tmax, Lart_gpt, Cart_gpt, dt, lbda_gpt, t_a)
	Cart_gpt, Lart_gpt, Tact_gpt = updateGuccioneVariable(Tmax, Lart_gpt, Cart_gpt, dt, lbda_gpt, t_a)
	print lbda_gpt
	#print Tact_gpt
	#print Lart_gpt
	#print Cart_gpt
	#print w.vector().array()[:]
	#print project(I + grad(w.split()[0]), QuadT).vector().array()[:]

	if(math.isnan(lbda_gpt[0])):
		break;


	Cart.vector()[:] = Cart_gpt
	Lart.vector()[:] = Lart_gpt
	Tact.vector()[:] = Tact_gpt

	w, nIter, eps = solver.solveLVP(w, W, bcs2, Jac, Ltotal, t_niter, r_threshold)


	print >>fdata, t_a, Tact_gpt[0], lbda_gpt[0], Cart_gpt[0], Lart_gpt[0], Tact_gpt[0]	

	writenodalresults(w, mesh, V, t_a)

	if(num_field > 1):
		disp_vtk_file << (w.split()[0], t_a)
		p_vtk_file << (w.split()[1], t_a)
	if(num_field > 2):
		disp_vtk_file << (w.split()[0], t_a)
		theta_vtk_file << (w.split()[2], t_a)
	if(num_field == 1):
		disp_vtk_file << (w, t_a)

    	#w0.vector()[:] = w.vector()

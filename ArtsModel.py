from dolfin import *
from math import *
import math as math
import os as os
import numpy as np
from edgetypebc import *
from addfiber import *
from matplotlib import pylab as plt
from edge import *

def ArtsActiveForce_Formulation(u, Carts, Larts, f0, s0, n0):

	# ARTS MODEL #########################################################################################################################
	Crest = Constant(0.00)
	Lsc0 = Constant(1.51)
	Lsref = Constant(2.0)
	Lseiso = Constant(0.04)
	vmax = Constant(7.0e-3)
	tauD = Constant(32.0) # 32.0 # 100.0 # 32.0 # 33.8 #  
	tauR = Constant(48.0) # 28.1 #48.0 #28.1*3 # 48.0 # 28.1 #  
	tauSC = Constant(425.0) # 250.0 #  425.0
	bh = Constant(1.5)
	ah = Constant(1.5)

	F = I + grad(u)             # Deformation gradient
	Cmat = F.T*F       	    # Right Cauchy-Green tensor
	Lbda = sqrt(dot(f0, Cmat*f0))

	f_vec = F*f0/sqrt(inner(F*f0, F*f0))
	n_vec = F*s0/sqrt(inner(F*n0, F*n0))

 	Ls = Lbda*Lsref
	xp1 = conditional(gt(t_a/tauR,0.0),t_a/tauR,0.0)
	xp = conditional(lt(xp1,8), xp1, 8)

	Frise = 0.02*(xp**3.0)*(8.0 - xp)**2.0*exp(-xp);
	Lsnorm = (Ls - Larts)/Lseiso
	#p1 = Constant(1.0)/(Constant(1.0) + exp(Constant(-50)*(Lsnorm - Constant(1.0))))
	#p2 = Constant(1.0)/(Constant(1.0) + exp(Constant(50)*(Lsnorm - Constant(1.0))))
	#dLarts = Larts0 + dt*(p1*(Lsnorm - 1.0)/(bh*Lsnorm + 1.0)*vmax + p2*(Lsnorm - 1.0)/(bh*Lsnorm + 1.0)*vmax*exp(ah*(Lsnorm - 1.0)))
	
	#dLarts = (Larts0 + dt*vmax*(Ls/Lseiso - 1.0))/(1.0 + dt*vmax/Lseiso)
	dLarts = Larts0 + dt*((Ls - Larts)/Lseiso*vmax - Constant(1.0))

	TLsc = tauSC*(0.29 + 0.3*(Larts))
	#CLsc = conditional(ge(dLarts, Lsc0), tanh(20.0*(Larts - Lsc0)**2.0), Constant(0.0))
	CLsc = tanh(20.0*(Larts - Lsc0)**2.0)
	coef = (1.0 + exp((TLsc - t_a)/tauD))

	#dCarts = 1.0 / (1.0 + dt/(tauD * coef))*(Carts0 + dt/tauR * CLsc * Frise)

	#Tact =  Tmax  * Frise 
	p1 = conditional(gt(Ls,Larts), 1.0, -0.1)
	#p2 = conditional(gt(dLarts, Lsc0), 1.0, 0.0)
	Tact =  Tmax  * Carts * (Larts - Lsc0) * (Ls -  Larts) / Lseiso #* p1
	#Tact =  conditional(gt(dLarts, Lsc0), Tmax * Carts * (Larts - Lsc0) * (Ls -  Larts) / (Lseiso), Constant(0.0))
	#Tact = Tmax * Carts * (Larts - Lsc0) * Lsnorm * Constant(1.0)/(Constant(1.0) + exp(Constant(-50)*(Lsnorm)))

	gradv_ff = inner(f0, grad(v)*f0)
	gradv_ss = inner(n0, grad(v)*n0)

	Lactive1 = inner(Tact, gradv_ff)*dx(domain=mesh)
	#Lactive += inner(0.4*Tact, gradv_ss)*dx(domain=mesh)
	#Lactive2 = inner(carts, Carts - dCarts)*dx(domain=mesh)
	Lactive2 = inner(carts, Carts - Carts0 - (dt/tauR)*Frise*CLsc + (dt/tauD)*Carts/(Constant(1.0) + exp((TLsc - t_a)/tauD)))*dx(domain=mesh)
	Lactive3 = inner(larts, Larts - Larts0 - dt*vmax*((Ls - Larts)/Lseiso - Constant(1.0)))*dx(domain=mesh)

	Lactive = Lactive1 + Lactive2 + Lactive3

	Jactive1 = Tmax * Carts * (Larts - Lsc0) * (Lsref  / Lseiso) * Constant(0.5)/Lbda * inner(f0, (grad(Du) + grad(Du).T + grad(u).T*grad(Du) + grad(Du).T*grad(u))*f0) 
	Jactive1 += Tmax * (Carts / Lseiso) * (Ls * DLarts - Constant(2.0) * Larts * DLarts  + DLarts * Lsc0) 
        Jactive1 += Tmax * DCarts * (Larts - Lsc0) * (Ls -  Larts) / Lseiso

	Jactive = derivative(Lactive2 + Lactive3, w, TrialFunction(W)) 
	#Jactive += Jactive1 * gradv_ff *  dx(domain=mesh) #* p1
	Jactive += derivative(Lactive1, w, TrialFunction(W))

	return Lactive, Jactive
	#####################################################################################################################################

def ArtsModel(mesh, endoid, epiid, facetboundaries, subdomain):

	V = VectorFunctionSpace(mesh, "Lagrange", 2)
	Q = FunctionSpace(mesh, "Lagrange",1)
	Q2 = FunctionSpace(mesh, "Lagrange",1)
	DG = FunctionSpace(mesh, "DG",0)
	Pspace = FunctionSpace(mesh, "Real", 0) 
	W = MixedFunctionSpace([V,DG,Q,Q,Q,Pspace])
	
	if(dolfin.dolfin_version() == '1.7.0dev'):
		Quadelem = VectorElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, dim=3, quad_scheme='default')
		Quad = FunctionSpace(mesh, Quadelem)
	
		Quadscalar = FiniteElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme='default')
		Quads = FunctionSpace(mesh, Quadscalar)
	
	else:
		Quad = VectorFunctionSpace(mesh, "Quadrature", quad_degree)
		Quads = FunctionSpace(mesh, "Quadrature", quad_degree)
	
	f0, s0, n0 = addfiber(mesh, Quad, fibercasename)
	File("fiber.pvd") << project(f0,V)
	File("sheet.pvd") << project(s0,V)
	File("sheetnormal.pvd") << project(n0,V)
	
	
	## Define variational problem
	w = Function(W)
	w0 = Function(W)
	w_prev = Function(W)
	dw = TrialFunction(W)
	(u,p,Theta,Carts,Larts,Pendo) = split(w);
	(u0,p0,Theta0,Carts0,Larts0,Pendo0) = split(w0);
	(Du,Dp,DTheta,DCarts,DLarts,DPendo) = split(dw);
	
	w.interpolate(InitialConditions(degree=1))
	w_prev.interpolate(InitialConditions(degree=1))
	w0.interpolate(InitialConditions(degree=1))
	
	v,q,theta,carts,larts,pendo = TestFunctions(W)
	
	d = u.geometric_dimension()
	X = SpatialCoordinate(mesh)
	I = Identity(d)             # Identity tensor
	F = I + grad(u)             # Deformation gradient
	J = det(F)
	Fdev = J**(-1.0/3.0)*F
	Cmat = F.T*F       	    # Right Cauchy-Green tensor
	Cdev = Fdev.T*Fdev
	E = 0.5*(Cmat - I)
	Edev = 0.5*(Cdev - I)
	Finv = inv(F)
	Qmat = Qtransform(f0, s0, n0)
	Eloc = (Qmat)*E*(Qmat.T)
	Edevloc = (Qmat)*Edev*(Qmat.T)
	n = cofac(F)*N
	Kspring = 2e3
	
	V0 = Expression("vol", vol=cavityvol(N,w,X,endoid), degree = 1)
	
	Lvol = inner_volume_constraint(u, Pendo, V0, facetboundaries, endoid)
	Lpassive = PassiveFungModel(u, f0, s0, n0)
	Lactive, Jactive = ArtsActiveForce_Formulation(u, Carts, Larts, f0, s0, n0)
	L = Lpassive + Lvol
	Ltotal = L + Lactive 
	
	# Compute Jacobian of F
	Jac = derivative(L, w, TrialFunction(W)) + Jactive 



from dolfin import *

def GuccioneActiveForce_Formulation(w, w0, dw, wtest, f0, s0, n0, t_a, dt, Tmax, mesh, isvolctrl):

	if(isvolctrl):
		(u,p,Theta,Carts,Larts,Pendo) = split(w);
		(u0,p0,Theta0,Carts0,Larts0,Pendo0) = split(w0);
		(Du,Dp,DTheta,DCarts,DLarts,DPendo) = split(dw);
		(v,q,theta,carts,larts,pendo) = split(wtest)
	else:
		(u,p,Theta,Carts,Larts) = split(w);
		(u0,p0,Theta0,Carts0,Larts0) = split(w0);
		(Du,Dp,DTheta,DCarts,DLarts) = split(dw);
		(v,q,theta,carts,larts) = split(wtest)

	# Guccione MODEL #########################################################################################################################
	Ca0 = Constant(4.35)
	Ca0Max = Constant(4.35)
	B = Constant(4.75)
	L0 = Constant(1.58)
	t0 = Constant(100)
	m = Constant(1.0849e3)
	b = Constant(-1.429e3)
	LR = Constant(1.85)
        n = Constant(2.5);

	d = u.geometric_dimension()
	I = Identity(d)             # Identity tensor
	F = I + grad(u)             # Deformation gradient
	Cmat = F.T*F       	    # Right Cauchy-Green tensor
	Lbda = sqrt(dot(f0, Cmat*f0))

	sacL = Lbda*LR

	#tr = 200#m*L + b
	#tr = m*sacL + b
	##wt = conditional(lt(t_a, t0), pi*t_a/t0, conditional(gt(t_a, t0 + tr), pi*((t_a - t0)/tr + 1.0), 0.0))
	#p1 = conditional(lt(t_a, t0), 1.0, 0)
	#p2a = conditional(ge(t_a, t0), 1.0, 0)
	##p2b = conditional(le(t_a, t0 + tr), 1.0, 0)
	#wt = p1*(pi*t_a/t0) + p2a*(pi*(t_a - t0 + tr)/tr)
	#Ct = 0.5*(1.0 - cos(wt))

	taur = conditional(gt(sacL,L0),3.0/((m*sacL + b - t0)**n), t0)
	p1 = conditional(lt(t_a, t0), exp(-0.4e-3*(t_a - t0)**2.0), 0)
	p2 = conditional(ge(t_a, t0), exp(-taur*((abs(t_a - t0))**n)), 0)
	Ct = p1 + p2

	Coeff = conditional(gt(sacL,L0), (exp(B*(sacL - L0)) - 1.0), 0.0)
	Tact = Tmax * (Ca0**2.0) *Coeff / ((Ca0**2.0) * Coeff + Ca0Max**2.0) * Ct
	
	gradv_ff = inner(f0, grad(v)*f0)

	Lactive1 = inner(Tact, gradv_ff)*dx(domain=mesh)
	Lactive2 = inner(carts, Carts - Carts0)*dx(domain=mesh)
	Lactive3 = inner(larts, Larts - Larts0)*dx(domain=mesh)

	Lactive = Lactive1 + Lactive2 + Lactive3

	Jactive = derivative(Lactive, w, dw)

	return Lactive, Jactive
	#####################################################################################################################################

	#####################################################################################################################################


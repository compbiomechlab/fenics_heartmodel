from dolfin import *
from math import *
import os 
import math as math
import numpy as np
from edgetypebc import *
from addfiber import *
from matplotlib import pylab as plt
from edge import *

os.system("rm *.pvd")
os.system("rm *.vtu")

quad_degree = 6

dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3'
dolfin.parameters["form_compiler"]["cpp_optimize"] = True
dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["quadrature_degree"] = quad_degree
dolfin.parameters["form_compiler"]["representation"] = "uflacs"
dolfin.parameters['allow_extrapolation'] = True

class InitialConditions(Expression):
    def eval(self, values, x):
        values[0] = 0.0 
        values[1] = 0.0 
        values[2] = 0.0 
        values[3] = 0.0 
        values[4] = 0.0
    
    def value_shape(self):
        return (5,)

def solveLVP(w, volume, endoid):

    V0.vol = volume

    w0.vector()[:] = w.vector()
    solver.solve(problem, w.vector())

    LVcavvol = cavityvol(N,w,X, endoid)
    LVwallvol = wallvol(N,w,X)
    LVP = cavitypressure(W, w)
    niter =  solver.iteration()

    return LVP, LVwallvol, LVcavvol, niter

def solveLVP2(w, volume, bcs, Jac, F, endoid):

    test_file = File("test.pvd")
    Tacttest_file = File("Tacttest.pvd")
    Coefftest_file = File("Coefftest.pvd")
    Lbdatest_file = File("Lbdatest.pvd")
    Cttest_file = File("Cttest.pvd")
    taurtest_file = File("taurtest.pvd")
    V0.vol = volume

    bcs_du = bcs
    w_inc = Function(W)
    nIter = 0
    eps = 1

    while eps > 1e-7 and nIter < 20:              # Newton iterations
        nIter += 1
        A, b = assemble_system(Jac, -F, bcs_du)
        solve(A, w_inc.vector(), b, "umfpack")
        eps = np.linalg.norm(w_inc.vector().array(), ord = 2)

        fnorm = b.norm('l2')
        lmbda = 1.0     

	if(t_a.time == 86):
		test_file << (w.sub(0), float(nIter))
		Tact, coeff, Lbda, Ct, taur = GuccioneActiveForce(w.sub(0), f0)
		Tacttest_file << (project(Tact,Q), float(nIter))
		Coefftest_file << (project(coeff,Q), float(nIter))
		Lbdatest_file << (project(Lbda,Q), float(nIter))
		Cttest_file << (project(Ct,Q), float(nIter))
		taurtest_file << (project(taur,Q), float(nIter))

        w.vector()[:] += lmbda*w_inc.vector()    # New u vector
        print '      {0:2d}  {1:3.2E}  {2:5e}'.format(nIter, eps, fnorm)

    LVcavvol = cavityvol(N,w,X, endoid)
    LVwallvol = wallvol(N,w,X)
    LVP = cavitypressure(W, w)

    return LVP, LVwallvol, LVcavvol, nIter, eps

def GuccioneActiveForce_Formulation(Cmatloc, f0,  du):

	# Guccione MODEL #########################################################################################################################
	Ca0 = 4.35
	Ca0Max = 4.35
	B = 4.75
	L0 = 1.58
	t0 = 100
	m = 1.0849e3
	b = -1.429e3
	LR = 1.85
        n = 2.0;

	F = I + grad(u)             # Deformation gradient
	Cmat = F.T*F       	    # Right Cauchy-Green tensor
	Lbda = sqrt(dot(f0, Cmat*f0))

	sacL = Lbda*LR

	#tr = 200#m*L + b
	#tr = m*sacL + b
	##wt = conditional(lt(t_a, t0), pi*t_a/t0, conditional(gt(t_a, t0 + tr), pi*((t_a - t0)/tr + 1.0), 0.0))
	#p1 = conditional(lt(t_a, t0), 1.0, 0)
	#p2a = conditional(ge(t_a, t0), 1.0, 0)
	##p2b = conditional(le(t_a, t0 + tr), 1.0, 0)
	#wt = p1*(pi*t_a/t0) + p2a*(pi*(t_a - t0 + tr)/tr)
	#Ct = 0.5*(1.0 - cos(wt))

	taur = conditional(gt(sacL,L0),3.0/((m*sacL + b - t0)**n), 100.0)
	p1 = conditional(lt(t_a, t0), exp(-0.4e-3*(t_a - t0)**2.0), 0)
	p2 = conditional(ge(t_a, t0), exp(-taur*((abs(t_a - t0))**n)), 0)
	Ct = p1 + p2

	Coeff = conditional(gt(sacL,L0), (exp(B*(sacL - L0)) - 1.0), 0.0)
	Tact = Tmax * (Ca0**2.0) *Coeff / ((Ca0**2.0) * Coeff + Ca0Max**2.0) * Ct
	
	Tmat = (Qmat.T)*as_matrix([[Tact, 0, 0], [0, 0, 0], [0, 0, 0]])*Qmat

	Lactive = inner(Tmat, grad(v))*dx(domain=mesh)
	Jactive = derivative(Lactive, u, du)

	return Lactive, Jactive, Tact
	#####################################################################################################################################

def GuccioneActiveForce(u, f0):

	# Guccione MODEL #########################################################################################################################
	Ca0 = 4.35
	Ca0Max = 4.35
	B = 4.75
	L0 = 1.58
	t0 = 100
	m = 1.0849e3
	b = -1.429e3
	LR = 1.85
        n = 2.0;

	F = I + grad(u)             # Deformation gradient
	Cmat = F.T*F       	    # Right Cauchy-Green tensor
	Lbda = sqrt(dot(f0, Cmat*f0))

	sacL = Lbda*LR

	#tr = 200#m*L + b
	#tr = m*L + b
	#wt = conditional(lt(t_a, t0), pi*t_a/t0, conditional(gt(t_a, t0 + tr), pi*((t_a - t0)/tr + 1.0), 0.0))
	#p1 = conditional(lt(t_a, t0), 1.0, 0)
	#p2a = conditional(ge(t_a, t0), 1.0, 0)
	#p2b = conditional(le(t_a, t0 + tr), 1.0, 0)
	#wt = p1*(pi*t_a/t0) + p2a*(pi*(t_a - t0 + tr)/tr)
	#Ct = 0.5*(1.0 - cos(wt))

	taur = conditional(gt(sacL,L0),3.0/((m*sacL + b - t0)**n), 100.0)
	p1 = conditional(lt(t_a, t0), exp(-0.4e-3*(t_a - t0)**2.0), 0)
	p2 = conditional(ge(t_a, t0), exp(-taur*((t_a - t0)**n)), 0)
	Ct = p1 + p2

	Coeff = conditional(gt(sacL,L0), (exp(B*(sacL - L0)) - 1.0), 0.0)
	Tact = Tmax * (Ca0**2.0) *Coeff / ((Ca0**2.0) * Coeff + Ca0Max**2.0) * Ct
	
	return Tact, Coeff, Lbda, Ct, taur
	#####################################################################################################################################


def PassiveFungModel(Eloc):

	bf = 29.9;
	bfx = 13.3;
	bxx  =13.5;
	Ccoeff = 50;
        Kappa = 1e2;

	Q = bf*Eloc[0,0]**2 + bxx*(Eloc[1,1]**2 + Eloc[2,2]**2 + Eloc[1,2]**2 + Eloc[2,1]**2) + bfx*(Eloc[0,1]**2 + Eloc[1,0]**2 + Eloc[0,2]**2 + Eloc[0,2]**2)
	psi = Ccoeff*(exp(Q) - 1) - p*(J - 1);
        # Nearly Incompressible
	#psi = Ccoeff*(exp(Q) - 1) - Kappa*(J - 1)**2.0;

	return derivative(psi, u, v)*dx(domain=mesh)


def inner_volume_constraint(u, Pendo, V, bfun, sigma):

        """
        Compute the form
            (V(u) - V, p) * ds(sigma)
        where V(u) is the volume computed from u and
            u = displacement
            V = volume enclosed by sigma
            p = Lagrange multiplier
        sigma is the boundary of the volume.
        """

        #dom = u.ufl_domain()
	dom = mesh
        X = SpatialCoordinate(mesh)
        N = FacetNormal(mesh)
        # ufl doesn't support any measure for duality
        # between two Real spaces, so we have to divide
        # by the total measure of the domain
        ds_sigma = ds(sigma, domain = dom, subdomain_data = bfun)
        area = assemble(Constant(1.0) * ds_sigma)

        # in detail, we impose the constraint
        #   ( 1/dim (x, n) + 1/area V ) * p = 0
        x = X + u
        F = grad(x)
        n = cofac(F)*N

        V_u = - Constant(1.0/3.0) * inner(x, n)
        psi_vol = - Pendo * V_u * ds_sigma

        if V is not None :
            psi_vol += Constant(1.0/area) * Pendo * V * ds_sigma

	Lvol = derivative(psi_vol, u, v) + derivative(psi_vol, Pendo, pendo)

        return Lvol

def cavityvol(N,w,X, endoid):

    vol_form = -1.0/3.0 * inner(N,X+w.split()[0])*ds(endoid)
    vol = assemble(vol_form)

    return vol

def wallvol(N,w,X):

    meshvol_form = 1/float(3) * inner(N,X+w.split()[0])*ds
    meshvol = assemble(meshvol_form)

    return meshvol

def cavitypressure(W, w):

    dofmap = W.sub(2).dofmap()
    # Nearly Incompressible
    #dofmap = W.sub(3).dofmap()
    val_dof = dofmap.cell_dofs(0)[0]
    pressure = w.vector()[val_dof][0]

    return pressure


# Class for interfacing with the Newton solver
class PoroelasticityEquation(NonlinearProblem):
    def __init__(self, a, L, bcs):
        NonlinearProblem.__init__(self)
        self.L = L
        self.a = a
	self.bcs = bcs
	self.ffc_options = {"optimize": True, \
               "eliminate_zeros": True, \
               "precompute_basis_const": True, \
               "precompute_ip_const": True}

    def F(self, b, x):
        assemble(self.L, tensor=b)
	for bc in bcs: bc.apply(b,x)
    def J(self, A, x):
        assemble(self.a, tensor=A)
	for bc in bcs: bc.apply(A)
	self.reset_sparsity = False

def Qtransform(f0, s0, n0):

	Q = as_matrix([[f0[0], f0[1], f0[2]], [s0[0], s0[1], s0[2]], [n0[0], n0[1], n0[2]]]);

	return Q

	

t_a = Expression("time", time = 0.0, degree = 1)
dt = Expression("value", value = 10, degree = 1)
Tmax = Expression("value", value = 100000, degree = 1)
Kspring = 1e6
tstep = 0;

# Geometry ------------------------------------------------------------------
#topid = 1
#endoid = 2
#mesh = Mesh("./mesh/ellipsoidal.xml")
#n = FacetNormal ( mesh )
#facetboundaries = MeshFunction('size_t', mesh, './mesh/ellipsoidal_facet_region.xml')
#subdomains = MeshFunction("size_t", mesh, "./mesh/ellipsoidal_physical_region.xml")
#rfun = MeshFunction("size_t", mesh, 1)
#rfun.set_all(0)
#bl = CompiledSubDomain("(x[1]*x[1] + x[0]*x[0]) < 3.5*3.5 + 0.5 && (x[1]*x[1] + x[0]*x[0]) > 3.5*3.5 - 0.5 && near(x[2],0)")
#bl.mark(rfun, 10)
#plot(facetboundaries, interactive=True)

topid = 2
endoid = 3
casename = "./mesh/ellipsoidal/ellipsoidal_fc"
mesh = Mesh("./mesh/ellipsoidal/ellipsoidal_fc.xml")
n = FacetNormal ( mesh )
facetboundaries = MeshFunction('size_t', mesh, './mesh/ellipsoidal/ellipsoidal_fc_facet_region.xml')
subdomains = MeshFunction("size_t", mesh, "./mesh/ellipsoidal/ellipsoidal_fc_physical_region.xml")
rfun = MeshFunction("size_t", mesh, 1)
rfun.set_all(0)
bl = CompiledSubDomain("(x[1]*x[1] + x[0]*x[0]) > 3.5*3.5 - 1.5 && x[2]*x[2] < 0.5")
#bl = CompiledSubDomain("(x[1]*x[1] + x[0]*x[0]) < 3.5*3.5 +2 && x[2]*x[2] < 1")
#bl = CompiledSubDomain("(x[1]*x[1] + x[0]*x[0]) < 2.3*2.3 + 0.3 && (x[1]*x[1] + x[0]*x[0]) > 2.3*2.3 - 0.3 && near(x[2],0)")
bl.mark(rfun, 10)
#plot(facetboundaries, interactive=True)
File("Facetboundaries.pvd") << facetboundaries
#stop
#-----------------------------------------------------------------------------------------
#

V = VectorFunctionSpace(mesh, "Lagrange", 2)
Q = FunctionSpace(mesh, "Lagrange",1)
Pspace = FunctionSpace(mesh, "Real", 0) 
W = MixedFunctionSpace([V,Q,Pspace])

#Quadelem = VectorElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, dim=3, quad_scheme='default')
#Quad = FunctionSpace(mesh, Quadelem)
Quad = VectorFunctionSpace(mesh, "Quadrature", quad_degree)

f00, s00, n00 = addfiber(mesh, Quad, casename)
f0 = f00/sqrt(dot(f00,f00))
s0 = s00/sqrt(dot(s00,s00))
n0 = n00/sqrt(dot(n00,n00))
#n0 = cross(f0, s0)

# Output quad points -------------------------------------------------------
#e = FiniteElement("Quadrature", mesh.ufl_cell(), 2, quad_scheme='default')
#gdim = mesh.geometry().dim()
#xq = Quad.dofmap().tabulate_all_coordinates(mesh).reshape((-1, gdim))
## You might want to remove the duplicates
#xq0 = xq[Quad.sub(0).dofmap().dofs()]  
#print Quad.sub(0).dofmap().dofs()[0]
##print f00.vector()[Quad.sub(0).dofmap()]
#fdata = open("gausspoints.txt", "w")
#cnt = 0;
#for xq00 in xq0:
#	print >>fdata, xq00[0], xq00[1], xq00[2], \
#		f00.vector()[Quad.sub(0).dofmap().dofs()[cnt]][0], \
#		f00.vector()[Quad.sub(1).dofmap().dofs()[cnt]][0], \
#		f00.vector()[Quad.sub(2).dofmap().dofs()[cnt]][0],\
#		s00.vector()[Quad.sub(0).dofmap().dofs()[cnt]][0], \
#		s00.vector()[Quad.sub(1).dofmap().dofs()[cnt]][0], \
#		s00.vector()[Quad.sub(2).dofmap().dofs()[cnt]][0],\
#		n00.vector()[Quad.sub(0).dofmap().dofs()[cnt]][0], \
#		n00.vector()[Quad.sub(1).dofmap().dofs()[cnt]][0], \
#		n00.vector()[Quad.sub(2).dofmap().dofs()[cnt]][0]
#
#
#	cnt = cnt + 1;
#
#fdata.close()
# --------------------------------------------------------------------------
#stop


File("fiber.pvd") << project(f0,V)
File("sheet.pvd") << project(s0,V)
File("sheetnormal.pvd") << project(n0,V)


l = Expression(("0.0", "0.0", "0.0"), degree = 1)
press = Expression(("P"), P = 0, degree = 1)
bctop = DirichletBC(W.sub(0).sub(2), Expression(("0.0"), degree = 1), facetboundaries, topid)
endoring = pick_endoring_bc()(rfun, 10)
bc_epi_edge = DirichletBC(W.sub(0), l, endoring, method = "pointwise")
bcs = [bc_epi_edge, bctop]
#bcs = [bctop]

dx = Measure("dx",domain=mesh, subdomain_data=subdomains)
ds = Measure("ds")[facetboundaries]  
##################################################################################################################################

## Define variational problem
w = Function(W)
w0 = Function(W)
w_prev = Function(W)
dw = TrialFunction(W)
(u,p,Pendo) = split(w);
(u0,p0,Pendo0) = split(w0);
(du,dp,dPendo) = split(dw);

w.interpolate(InitialConditions(degree=1))
w_prev.interpolate(InitialConditions(degree=1))
w0.interpolate(InitialConditions(degree=1))

v,q,pendo = TestFunctions(W)

d = u.geometric_dimension()
X = SpatialCoordinate(mesh)
I = Identity(d)             # Identity tensor
F = I + grad(u)             # Deformation gradient
F0 = I + grad(u0)        
Cmat = F.T*F       	    # Right Cauchy-Green tensor
E = 0.5*(Cmat - I)
Ic = variable(tr(Cmat))
J = variable(det(F))
J0 = det(F0)
Finv = inv(F)
Qmat = Qtransform(f0, s0, n0)
Eloc = (Qmat)*E*(Qmat.T)
Cmatloc = (Qmat)*Cmat*(Qmat.T)
N = J*(Finv.T)*n


V0 = Expression("vol", vol=cavityvol(N,w,X, endoid), degree = 1)

# Stored strain energy density (compressible neo-Hookean model)
Lvol = inner_volume_constraint(u, Pendo, V0, facetboundaries, endoid)
Lpassive = PassiveFungModel(Eloc)
Lincomp = inner(q, J - 1.0)*dx(domain=mesh) 	#+ dt*inner(grad(p),0.01*grad(q))*dx 
Lactive, Jactive, Tact = GuccioneActiveForce_Formulation(u, f0, du)
L = Lpassive + Lvol + Lincomp  #+ inner(dot(v,n), Kspring*dot(u,n))*ds(endoid)
Ltotal = L + Lactive

# Compute Jacobian of F
Jac = derivative(Ltotal, w, TrialFunction(W)) #+ derivative(Lactive, w, TrialFunction(W))
#Jac += Jactive

problem = PoroelasticityEquation(Jac, Ltotal, bcs)
solver = NewtonSolver()
solver.parameters['absolute_tolerance'] = 1E-5 # 1E-7
solver.parameters['relative_tolerance'] = 1E-4 # 1E-6
solver.parameters['maximum_iterations'] = 30
solver.parameters['relaxation_parameter'] = 1.0
solver.parameters['linear_solver']='umfpack'
#solver.parameters['linear_solver']='mumps'

disp_vtk_file = File("nonlinearelasticity_disp.pvd")
Tact_vtk_file = File("nonlinearelasticity_Tact.pvd")
Lbda_vtk_file = File("nonlinearelasticity_Lbda.pvd")
P_vtk_file = File("nonlinearelasticity_P.pvd")
C_vtk_file = File("nonlinearelasticity_C.pvd")


tstep = 0;
dt.value = 4  # time step
BCL = 900.0
maxdt = 4
T = 5*BCL

disp_vtk_file << (w.split()[0], t_a.time)
pdata = open("PV.txt","w")

time_array = []
force_array = []

Cao = 10.0/1000.0;
Cven = 600.0/1000.0;
Vart0 = 500;
Vven0 = 3200.0;
Rao = 5.0*1000.0 ;
Rven = 4.5*1000.0;
#Rper = 120.0*1000.0;
Rper = 20.0*1000.0;
SV = 50;

#Loading phase 
V_ven = 3660 
V_art = 640
V_cav = V0.vol
Vtarget = 1.5*V0.vol
loadstep = 5 
p_cav = 0.0
dV = (Vtarget - V0.vol)/loadstep
Tmax.value = 0.0001

V_cav =  V0.vol
for p in range(0, 5):
	V_cav += 2
	print "Loading phase, V_cav = ", V_cav, p_cav
	p_cav, LVwallvol, LVcavvol, niter, eps = solveLVP2(w, V_cav, bcs, Jac, Ltotal, endoid);
	#disp_vtk_file << (w.split()[0], float(p))


#V_cav =  V0.vol 
Tmax.value = 50e3

V_ven_prev = V_ven
V_art_prev = V_art
V_cav_prev = V_cav
p_cav_prev = p_cav
w_prev.vector()[:] = w0.vector()

LVcav_array = [V_cav]
Pcav_array = [p_cav]
itercnt = 0

fig = plt.figure()
ax = fig.add_subplot(111)
li, = ax.plot(LVcav_array, Pcav_array)
fig.canvas.draw()
plt.show(block=False)

while (tstep < T):

	tstep += dt.value

        cycle = floor(tstep/BCL)
	t_a.time = tstep - cycle*BCL
	print "Cycle number = ", cycle
	print "cell time = ", t_a.time

	try:
       		p_cav, LVwallvol, LVcavvol, niter, eps = solveLVP2(w, V_cav, bcs, Jac, Ltotal, endoid);
	except RuntimeError:
		print "Reset"
		V_cav = V_cav_prev;
		V_art = V_art_prev;
		V_ven = V_ven_prev;
		p_cav = p_cav_prev

		print "V_cav =", V_cav
		print "V_art =", V_art
		print "V_ven =", V_ven
		print "p_cav =", p_cav

		w.vector()[:] = w_prev.vector()

		tstep -= dt.value
		dt.value = 0.5*dt.value


	#if(niter == 20 and eps > 1e-7):
	#	print "Reset"
	#	V_cav = V_cav_prev;
	#	V_art = V_art_prev;
	#	V_ven = V_ven_prev;
	#	p_cav = p_cav_prev

	#	print "V_cav =", V_cav
	#	print "V_art =", V_art
	#	print "V_ven =", V_ven
	#	print "p_cav =", p_cav

	#	w.vector()[:] = w_prev.vector()

	#	tstep -= dt.value
	#	dt.value = 0.5*dt.value

        w0.vector()[:] = w.vector()

	Part = 1.0/Cao*(V_art - Vart0);
        Pven = 1.0/Cven*(V_ven - Vven0);
        PLV = p_cav;

        print "P_ven = ",Pven;
        print "P_LV = ", PLV;
        print "P_art = ", Part;

        if(PLV <= Part):
              Qao = 0.0;
        else:
             Qao = 1.0/Rao*(PLV - Part);
        

        if(PLV >= Pven):
            Qmv = 0.0;
        else: 
            Qmv = 1.0/Rven*(Pven - PLV);
        

        Qper = 1.0/Rper*(Part - Pven);

        print "Q_mv = ", Qmv ;
        print "Q_ao = ", Qao ;
        print "Q_per = ", Qper ;

	V_cav_prev = V_cav
	V_art_prev = V_art
	V_ven_prev = V_ven
	p_cav_prev = p_cav
	w_prev.vector()[:] = w.vector()

        V_cav = V_cav + dt.value*(Qmv - Qao);
        V_art = V_art + dt.value*(Qao - Qper);
        V_ven = V_ven + dt.value*(Qper - Qmv);

        print "V_ven = ", V_ven;
        print "V_LV = ", V_cav;
        print "V_art = ", V_art;

	print >>pdata, tstep, LVcavvol, p_cav, LVwallvol 

	LVcav_array.append(LVcavvol)
	Pcav_array.append(p_cav)
	li.set_xdata(LVcav_array)
	li.set_ydata(Pcav_array)
	plt.xlim([min(LVcav_array)-10,max(LVcav_array)+10])
	plt.ylim([min(Pcav_array)-10,max(Pcav_array)+10])
        fig.canvas.draw()

	
	################ DIAGNOSTIC ################################################################
	F = I + grad(w.sub(0))            
	Cmat = F.T*F      	    
	Cmatloc = (Qmat)*Cmat*(Qmat.T)
	Tact, coeff, Lbdaa, Ct, taur = GuccioneActiveForce(u, f0)
	#print "Cmatloc[0,0] = ", project(sqrt(Cmatloc[0,0]),Q)(-0.905, -2.19, -5.16)
	print "Cmatloc[0,0] = ", project(sqrt(Cmatloc[0,0]),Q)(0.5,1.5,-1)
	#print "L = ", project(sqrt(Cmatloc[0,0])*1.85,Q)(-0.905, -2.19, -5.16)
	print "L = ", project(sqrt(Cmatloc[0,0])*1.85,Q)(0.5,1.5,-1)
	#print "Ta = ", project(Tact,Q)(-0.905, -2.19, -5.16)

        Lbda_field = project(sqrt(Cmatloc[0,0]),Q)
	Lbda_field.rename("Lbda", "Lbda")
	Cmat_field = project(Cmat, TensorFunctionSpace(mesh, 'Lagrange', 1))
	Cmat_field.rename("Cmat", "Cmat")
	#Tact_field = project(Tact, Q)
	#Tact_field.rename("Tact", "Tact")
	################ DIAGNOSTIC ################################################################

	disp_vtk_file << (w.split()[0], tstep)
	Lbda_vtk_file << (Lbda_field, tstep)
	P_vtk_file << (w.split()[1], tstep)
	C_vtk_file << (Cmat_field, tstep)
	#Tact_vtk_file << (Tact_field, tstep)

	#if(niter < 5):
	#	itercnt = itercnt + 1;

	#if(itercnt > 4 and dt.value < maxdt and niter < 5):
	#	dt.value = dt.value*2
	#	solver.parameters['relaxation_parameter'] = 1.0
	#	itercnt = 0;

	#if(dt.value < 1e-2):
	#	print "Timestep exceed minimum"
	#	break;



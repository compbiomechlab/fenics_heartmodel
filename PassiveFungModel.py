from dolfin import *
import utilities as util

def PassiveFungModel(w, wtest, f0, s0, n0, mesh, isvolctrl, num_field, Kappa, dx1, Tact):

	if(isvolctrl):
		if(num_field == 1):
			(u,Pendo) = split(w);
			(v,pendo) = split(wtest)

		elif(num_field == 2):
			(u,p,Pendo) = split(w);
			(v,q,pendo) = split(wtest)
		else:
			(u,p,Theta,Pendo) = split(w);
			(v,q,theta,pendo) = split(wtest)
	else:
		if(num_field == 1):
			u = w;
			v = wtest;

		elif(num_field == 2):
			(u,p) = split(w);
			(v,q) = split(wtest)

		else:
			(u,p,Theta) = split(w);
			(v,q,theta) = split(wtest)


	#bf = Constant(14.4);
	#bfx = Constant(0.7*14.4);
	#bxx  = Constant(0.4*14.4);
	#Ccoeff = Constant(115);
	Ccoeff = Constant(2000);
	bf = Constant(18.48);
	bfx = Constant(3.58);
	bxx = Constant(1.627);
	
	#Kappa = Constant(1.0e4);

	d = u.geometric_dimension()
	I = Identity(d)             # Identity tensor
	F = I + grad(u)             # Deformation gradient
	J = det(F)
	Fdev = J**(-1.0/3.0)*F
	Cmat = F.T*F       	    # Right Cauchy-Green tensor
	Cdev = Fdev.T*Fdev
	E = Constant(0.5)*(Cmat - I)
	Edev = Constant(0.5)*(Cdev - I)
	Qmat = util.Qtransform(f0, s0, n0)
	Eloc = (Qmat)*E*(Qmat.T)
	Edevloc = (Qmat)*Edev*(Qmat.T)


	I4 = inner(Cmat*f0, f0)
        Wactive = Tact/2.0 * (I4 - 1.0)


	QQ = bf*Edevloc[0,0]**2 + bxx*(Edevloc[1,1]**2 + Edevloc[2,2]**2 + Edevloc[1,2]**2 + Edevloc[2,1]**2) + bfx*(Edevloc[0,1]**2 + Edevloc[1,0]**2 + Edevloc[0,2]**2 + Edevloc[0,2]**2)

	if(num_field == 1):
		U = Kappa/Constant(4.0)*((J - 1.0)**2.0 + ln(J)**2.0);
		psi = Ccoeff*(exp(QQ) - Constant(1.0)) + U;
	elif(num_field == 2):
		psi = Ccoeff*(exp(QQ) - Constant(1.0)) + p*(J - Constant(1.0))
	else:
		U = Kappa/Constant(4.0)*((Theta - 1.0)**2.0 + ln(Theta)**2.0);
		psi = Ccoeff*(exp(QQ) - 1.0) + p*(J - Theta) + U ;
	#psi = Ccoeff*(tr(Cdev) - Constant(3.0)) + + p*(J - Theta) + U 

	#return derivative(psi, u, v)*dx1 + derivative(psi, p, q)*dx1 + derivative(psi, Theta, theta)*dx1 
	return derivative(psi + Wactive, w, wtest)*dx1



from dolfin import *
import vtk as vtk
from vtk_py import *

def addfiber(mesh, V, casename, endo_angle, epi_angle, casedir):


	fiberV = Function(V)
	sheetV = Function(V)
	sheetnormV = Function(V)

	endofilename = casedir + 'endocardium.stl'
	epifilename = casedir + 'epicardium.stl'

	endofilename = casedir + 'endo.stl'
	epifilename = casedir + 'epi.stl'

	pdata_endo = readSTL(endofilename)
	pdata_epi = readSTL(epifilename)

	# Quad points
	gdim = mesh.geometry().dim()
	xdofmap = V.sub(0).dofmap().dofs()
	ydofmap = V.sub(1).dofmap().dofs()
	zdofmap = V.sub(2).dofmap().dofs()

	if(dolfin.dolfin_version() == '1.7.0dev'):
		xq = V.tabulate_dof_coordinates().reshape((-1, gdim))
		xq0 = xq[xdofmap]  
	else:
		xq = V.dofmap().tabulate_all_coordinates(mesh).reshape((-1, gdim))
		xq0 = xq[xdofmap]  

	# Create an unstructured grid of Gauss Points
	points = vtk.vtkPoints()
	ugrid = vtk.vtkUnstructuredGrid()
	cnt = 0;
	for pt in xq0:
		points.InsertNextPoint(pt)
		cnt += 1

	ugrid.SetPoints(points)

	CreateVertexFromPoint(ugrid)
	addLocalProlateSpheroidalDirections(ugrid, pdata_endo, pdata_epi, type_of_support="cell")
	addLocalFiberOrientation(ugrid, endo_angle, epi_angle)

	fiber_vector =  ugrid.GetCellData().GetArray("fiber vectors")
	sheet_vector =  ugrid.GetCellData().GetArray("sheet vectors")
	sheetnorm_vector =  ugrid.GetCellData().GetArray("sheet normal vectors")

	# Temporary hack to switch fiber to homogeneous at apex
	#cellcentre = vtk.vtkCellCenters()
	#cellcentre.SetInput(ugrid)
	#cellcentre.Update()

	#minz = cellcentre.GetOutput().GetBounds()[4]
	#cnt = 0
	#for pt in xq0:

	#	fvec = fiber_vector.GetTuple(cnt)
	#	svec = sheet_vector.GetTuple(cnt)
	#	nvec = sheetnorm_vector.GetTuple(cnt)

	#	coord = cellcentre.GetOutput().GetPoints().GetPoint(cnt)
	#	if(coord[2] < minz + 0.5):
	#		fiber_vector.SetTuple3(cnt, 0, 0, 1)
	#		sheet_vector.SetTuple3(cnt, 0, 1, 0)
	#		sheetnorm_vector.SetTuple3(cnt, 1, 0, 0)

	#	cnt += 1
	##############################################################3

	cnt = 0
	for pt in xq0:

		fvec = fiber_vector.GetTuple(cnt)
		svec = sheet_vector.GetTuple(cnt)
		nvec = sheetnorm_vector.GetTuple(cnt)

		fvecnorm = sqrt(fvec[0]**2 + fvec[1]**2 + fvec[2]**2)
		svecnorm = sqrt(svec[0]**2 + svec[1]**2 + svec[2]**2)
		nvecnorm = sqrt(nvec[0]**2 + nvec[1]**2 + nvec[2]**2)

		if(abs(fvecnorm - 1.0) > 1e-7 or  abs(svecnorm - 1.0) > 1e-6 or abs(nvecnorm - 1.0) > 1e-7):
			print fvecnorm
			print svecnorm
			print nvecnorm

		#print xdofmap[cnt], ydofmap[cnt], zdofmap[cnt]
		fiberV.vector()[xdofmap[cnt]] = fvec[0]; fiberV.vector()[ydofmap[cnt]] = fvec[1]; fiberV.vector()[zdofmap[cnt]] = fvec[2];
		sheetV.vector()[xdofmap[cnt]] = svec[0]; sheetV.vector()[ydofmap[cnt]] = svec[1]; sheetV.vector()[zdofmap[cnt]] = svec[2];
		sheetnormV.vector()[xdofmap[cnt]] = nvec[0]; sheetnormV.vector()[ydofmap[cnt]] = nvec[1]; sheetnormV.vector()[zdofmap[cnt]] = nvec[2];

		cnt += 1





	# Read in a fine mesh
        #print "HERE"
	#finemesh = readXMLUGrid("../mesh/ellipsoidal/ellipsoidal_fc_fine.vtu")
	#finemesh_cellcenter = vtk.vtkCellCenters()
	#if(vtk.vtkVersion().GetVTKMajorVersion() > 5):
	#	finemesh_cellcenter.SetInputData(finemesh)
	#else:
	#	finemesh_cellcenter.SetInput(finemesh)
	#finemesh_cellcenter.Update()

	## Get Fiber field in fine mesh
	#fine_fiber_vector =  finemesh_cellcenter.GetOutput().GetPointData().GetArray("fiber vectors")
	#fine_sheet_vector =  finemesh_cellcenter.GetOutput().GetPointData().GetArray("sheet vectors")
	#fine_sheetnorm_vector =  finemesh_cellcenter.GetOutput().GetPointData().GetArray("sheet normal vectors")

	## Build KD Tree 
	#tree = vtk.vtkKdTreePointLocator();
  	#tree.SetDataSet(finemesh_cellcenter.GetOutput());
  	#tree.BuildLocator();
	
	#coarse_fiber_vector = vtk.vtkFloatArray()
	#coarse_fiber_vector.SetName("fiber vectors")
	#coarse_fiber_vector.SetNumberOfComponents(3)
	#coarse_sheet_vector = vtk.vtkFloatArray()
	#coarse_sheet_vector.SetName("sheet vectors")
	#coarse_sheet_vector.SetNumberOfComponents(3)
	#coarse_sheetnorm_vector = vtk.vtkFloatArray()
	#coarse_sheetnorm_vector.SetName("sheet normal vectors")
	#coarse_sheetnorm_vector.SetNumberOfComponents(3)

	#points = vtk.vtkPoints()
	#ugrid = vtk.vtkUnstructuredGrid()
	#cnt = 0;
	#for pt in xq0:
	#	ptid = tree.FindClosestPoint(pt)

	#	fvec = fine_fiber_vector.GetTuple(ptid)
	#	svec = fine_sheet_vector.GetTuple(ptid)
	#	nvec = fine_sheetnorm_vector.GetTuple(ptid)

	#	fvecnorm = sqrt(fvec[0]**2 + fvec[1]**2 + fvec[2]**2)
	#	svecnorm = sqrt(svec[0]**2 + svec[1]**2 + svec[2]**2)
	#	nvecnorm = sqrt(nvec[0]**2 + nvec[1]**2 + nvec[2]**2)

	#	if(abs(fvecnorm - 1.0) > 1e-6 or  abs(svecnorm - 1.0) > 1e-6 or abs(nvecnorm - 1.0) > 1e-6):
	#		print fvecnorm
	#		print svecnorm
	#		print nvecnorm

	#	#print xdofmap[cnt], ydofmap[cnt], zdofmap[cnt]
	#	fiberV.vector()[xdofmap[cnt]] = fvec[0]; fiberV.vector()[ydofmap[cnt]] = fvec[1]; fiberV.vector()[zdofmap[cnt]] = fvec[2];
	#	sheetV.vector()[xdofmap[cnt]] = svec[0]; sheetV.vector()[ydofmap[cnt]] = svec[1]; sheetV.vector()[zdofmap[cnt]] = svec[2];
	#	sheetnormV.vector()[xdofmap[cnt]] = nvec[0]; sheetnormV.vector()[ydofmap[cnt]] = nvec[1]; sheetnormV.vector()[zdofmap[cnt]] = nvec[2];

	#	coarse_fiber_vector.InsertNextTuple3(fvec[0], fvec[1], fvec[2])
	#	coarse_sheet_vector.InsertNextTuple3(svec[0], svec[1], svec[2])
	#	coarse_sheetnorm_vector.InsertNextTuple3(nvec[0], nvec[1], nvec[2])

	#	points.InsertNextPoint(pt)
	#	cnt += 1


	#ugrid.SetPoints(points)
	#ugrid.GetPointData().SetActiveVectors("fiber vectors")
	#ugrid.GetPointData().SetVectors(coarse_fiber_vector)
	#ugrid.GetPointData().SetActiveVectors("sheet vectors")
	#ugrid.GetPointData().SetVectors(coarse_sheet_vector)
	#ugrid.GetPointData().SetActiveVectors("sheet normal vectors")
	#ugrid.GetPointData().SetVectors(coarse_sheetnorm_vector)

	writeXMLUGrid(ugrid, "test.vtu")

	return fiberV, sheetV, sheetnormV



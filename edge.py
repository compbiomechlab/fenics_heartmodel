from dolfin import *

class Edge(SubDomain):

    def readcoord(self):

	data = open("./mesh/edge_node_coordinates.txt","r")
	lines = data.readlines()
	pt_array = []
	for line in lines:
		pt_array.append([float(line.strip().split()[0]), float(line.strip().split()[1]), float(line.strip().split()[2])])

	return pt_array

	
    def inside(self, x, on_boundary):
	b = 0;
	L = 0;
	TOL = 0.01;	
	pt_array = self.readcoord()

	ison = False 
	for pt in pt_array:
		if(abs(x[2]- pt[2]) <  TOL and abs(x[1] - pt[1]) <  TOL  and abs(x[0] -  pt[0]) < TOL):
			#print x[0], x[1], x[2], pt[0], pt[1], pt[2]
			ison = True
	return ison


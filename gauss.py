import vtk
from vtk_py import *

points = vtk.vtkPoints()

fdata = open("gausspoints.txt", "r")

fvec = vtk.vtkFloatArray()
fvec.SetName("fvec")
fvec.SetNumberOfComponents(3)

svec = vtk.vtkFloatArray()
svec.SetName("svec")
svec.SetNumberOfComponents(3)

nvec = vtk.vtkFloatArray()
nvec.SetName("nvec")
nvec.SetNumberOfComponents(3)



lines = fdata.readlines()
for line in lines:
	x = float(line.strip().split(' ')[0])
	y = float(line.strip().split(' ')[1])
	z = float(line.strip().split(' ')[2])

	fx = float(line.strip().split(' ')[3])
	fy = float(line.strip().split(' ')[4])
	fz = float(line.strip().split(' ')[5])

	sx = float(line.strip().split(' ')[6])
	sy = float(line.strip().split(' ')[7])
	sz = float(line.strip().split(' ')[8])

	nx = float(line.strip().split(' ')[9])
	ny = float(line.strip().split(' ')[10])
	nz = float(line.strip().split(' ')[11])

	fvec.InsertNextTuple([fx, fy, fz])
	svec.InsertNextTuple([sx, sy, sz])
	nvec.InsertNextTuple([nx, ny, nz])

	points.InsertNextPoint([x,y,z])

pdata = vtk.vtkPolyData()
pdata.SetPoints(points)
pdata.GetPointData().SetActiveVectors("fvec")
pdata.GetPointData().SetVectors(fvec)

pdata.GetPointData().SetActiveVectors("svec")
pdata.GetPointData().SetVectors(svec)

pdata.GetPointData().SetActiveVectors("nvec")
pdata.GetPointData().SetVectors(nvec)



writePData(pdata, "gausspoint.vtk")


from dolfin import *
def inner_volume_constraint(w, w0, dw, wtest, V0, bfun, endoid, mesh):

        """
        Compute the form
            (V(u) - V, p) * ds(endoid)
        where V(u) is the volume computed from u and
            u = displacement
            V0 = volume enclosed by endoid 
            p = Lagrange multiplier
        endoid is the boundary of the volume.
        """

	#(u,p,Theta,Carts,Larts,Pendo) = split(w);
	#(u0,p0,Theta0,Carts0,Larts0,Pendo0) = split(w0);
	#(Du,Dp,DTheta,DCarts,DLarts,DPendo) = split(dw);
	#(v,q,theta,carts,larts,pendo) = split(wtest)

	(u,p,Pendo) = split(w);
	(u0,p0,Pendo0) = split(w0);
	(Du,Dp,DPendo) = split(dw);
	(v,ptest,pendo) = split(wtest)

	dom = mesh
        X = SpatialCoordinate(mesh)
        N = FacetNormal(mesh)
        ds_endoid = ds(endoid, domain = dom, subdomain_data = bfun)
        area = assemble(Constant(1.0) * ds_endoid)

        # in detail, we impose the constraint
        #   ( 1/dim (x, n) + 1/area V ) * p = 0
        x = X + u
        F = grad(x)
        n = cofac(F)*N

        V_u = - Constant(1.0/3.0) * inner(x, n)
        psi_vol = - Pendo * V_u * ds_endoid

        if V0 is not None :
            psi_vol += Constant(1.0/area) * Pendo  * V0 * ds_endoid


        return psi_vol



import vtk
import numpy as np
from vtk_py import *
import sys
import os

def writexmldatafile(data, filename, fieldname, cellcenter):


	fiberfile = open(filename, "w")
	#sheetfile = open("sheet.xml", "w")
	#sheetnormfile = open("sheetnorm.xml","w")
	
	print >>fiberfile, "<?xml version='1.0'?>"
	print >>fiberfile, "<dolfin xmlns:dolfin='http://fenicsproject.org'>"
	print >>fiberfile, "<function_data size='%s'>"%(cellcenter.GetNumberOfPoints()*3)

	#print >>sheetfile, "<?xml version='1.0'?>"
	#print >>sheetfile, "<dolfin xmlns:dolfin='http://fenicsproject.org'>"
	#print >>sheetfile, "<function_data size='%s'>"%(cellcenter.GetNumberOfPoints()*3)

	for p in range(0, data.GetNumberOfPoints()):
		print >>fiberfile, "<dof index='%s' value='%s' cell_index='%s' cell_dof_index='%s' />"%(p*3,\
		                   data.GetPointData().GetArray(fieldname).GetTuple3(p)[0],p,0) 
		print >>fiberfile, "<dof index='%s' value='%s' cell_index='%s' cell_dof_index='%s' />"%(p*3+1,\
		                   data.GetPointData().GetArray(fieldname).GetTuple3(p)[1],p,1) 
		print >>fiberfile, "<dof index='%s' value='%s' cell_index='%s' cell_dof_index='%s' />"%(p*3+2,\
		                   data.GetPointData().GetArray(fieldname).GetTuple3(p)[2],p,2) 

		#print >>sheetfile, cellcenter.GetPointData().GetArray('sheet vectors').GetTuple3(p)[0],\
		#		   cellcenter.GetPointData().GetArray('sheet vectors').GetTuple3(p)[1],\
		#		   cellcenter.GetPointData().GetArray('sheet vectors').GetTuple3(p)[2]

		#print >>sheetnormfile, cellcenter.GetPointData().GetArray('sheet normal vectors').GetTuple3(p)[0],\
		#		       cellcenter.GetPointData().GetArray('sheet normal vectors').GetTuple3(p)[1],\
	#			       cellcenter.GetPointData().GetArray('sheet normal vectors').GetTuple3(p)[2]

	print >>fiberfile, "</function_data>"
	print >>fiberfile, "</dolfin>"


def write_stimfile(pdata, filename, isrotate, zoffset):

	filedata = open(filename,"w")

	print >>filedata, pdata.GetNumberOfPoints()

	for p in range(0, pdata.GetNumberOfPoints()):
		pt = pdata.GetPoints().GetPoint(p)
		if(isrotate):
			print >>filedata, "0", " " , (-pt[2]+zoffset)/1.0, " " , pt[1]/1.0, " " , pt[0]/1.0, " " , 0.01,  " " , -100, " ", "300 310 0 100"
		else:
			print >>filedata, "0", " " , pt[0], " " , pt[1], " " , pt[2], " " , 0.01,  " " , -100, " ", "300 305 0 100"

	filedata.close()


def gettetra(mesh):

	cta = vtk.vtkIntArray()
	cta.SetName("cell types")
	cta.SetNumberOfComponents(1)
	cta.SetNumberOfTuples(mesh.GetNumberOfCells())
	for x in xrange(0,mesh.GetNumberOfCells()):
	  cta.SetValue(x,mesh.GetCell(x).GetCellType())

	mesh.GetCellData().AddArray(cta)

	threshold = vtk.vtkThreshold()
	threshold.ThresholdBetween(9,11)
	if(vtk.vtkVersion.GetVTKMajorVersion() < 6):
		threshold.SetInput(mesh)
	else:
		threshold.SetInputData(mesh)
	threshold.SetInputArrayToProcess(0, 0, 0, 1, "cell types");
  	threshold.Update();

	return threshold.GetOutput()



def generate_infarct(mesh, pt, r, rBZ):

	center = vtk.vtkCellCenters()
	if(vtk.vtkVersion.GetVTKMajorVersion() < 6):
		center.SetInput(mesh)
	else:
		center.SetInputData(mesh)
	center.Update()

	isinfarct = vtk.vtkFloatArray()
	isinfarct.SetNumberOfComponents(1)
	isinfarct.SetName("Is Infarct")
	for p in range(0, center.GetOutput().GetNumberOfPoints()):
		cellcenter = center.GetOutput().GetPoints().GetPoint(p)
		if((vtk.vtkMath.Distance2BetweenPoints(pt, cellcenter)) < r**2):
			isinfarct.InsertNextValue(0);
		elif((vtk.vtkMath.Distance2BetweenPoints(pt, cellcenter)) >=  r**2 and (vtk.vtkMath.Distance2BetweenPoints(pt, cellcenter)) < rBZ**2):
			isinfarct.InsertNextValue(2);
		else:
			isinfarct.InsertNextValue(1);
	
	mesh.GetCellData().SetActiveScalars("Is Infarct")
	mesh.GetCellData().SetScalars(isinfarct)

	return mesh


def write_infarct(mesh, contractility, filename):

	
	filedata = open(filename,"w")
	for p in range(0, mesh.GetNumberOfCells()):
		print >>filedata, p, " ", mesh.GetCellData().GetArray("Is Infarct").GetValue(p)

	filedata.close()




def renumber2ndOrder(ugrid):
	newcellarray = vtk.vtkCellArray()
	newugrid = vtk.vtkUnstructuredGrid()


	for i in range(0, ugrid.GetNumberOfCells()):
		ptlist = vtk.vtkIdList()
		ugrid.GetCellPoints(i, ptlist)

		tetra = vtk.vtkQuadraticTetra()
		for p in range(0, 8):
			tetra.GetPointIds().SetId(p, int(ptlist.GetId(p)))
		tetra.GetPointIds().SetId(8, int(ptlist.GetId(9)))
		tetra.GetPointIds().SetId(9, int(ptlist.GetId(8)))

		newcellarray.InsertNextCell(tetra)

	newugrid.SetPoints(ugrid.GetPoints())
	newugrid.SetCells(tetra.GetCellType(), newcellarray)

	return newugrid





def create_mesh(meshsize, casename):

	is2ndorder = False
	meshfilename = casename + ".vtk"
	meshfilename_msh = casename + ".msh"
	meshfilename_vtu = casename + ".vtu"
	fiberfilename_xml = casename + "_fiber.xml"
	sheetfilename_xml = casename + "_sheet.xml"
	sheetnormfilename_xml = casename + "_sheetnorm.xml"

	os.system('cp ellipsoidal.geo ellipsodial_temp.geo')
	
	if(is2ndorder):
		cmd = "echo 'Mesh.ElementOrder = 2;' | cat - ellipsodial_temp.geo > temp && mv temp ellipsodial_temp.geo"
		os.system(cmd);
	
	cmd = "sed -i 's/<<Meshsize>>/"+"%5.3f"%(meshsize)+"/g' ellipsodial_temp.geo"
	os.system(cmd)
	cmd = "gmsh -3 ellipsodial_temp.geo -o %s"%(meshfilename)
	os.system(cmd)
	cmd = "gmsh -3 ellipsodial_temp.geo -o %s"%(meshfilename_msh)
	os.system(cmd)
	cmd = "rm ellipsodial_temp.geo"
	os.system(cmd)
	
	
	# Convert vtk to vtu file
	reader = vtk.vtkUnstructuredGridReader()
	reader.SetFileName(meshfilename)
	reader.Update()
	
	if(is2ndorder):
		ugrid = renumber2ndOrder(reader.GetOutput());
	else:
		ugrid = reader.GetOutput();
	
	# Get tetrahedron
	ugrid = gettetra(ugrid)
	
	# Generate infarct
	extracted = generate_infarct(ugrid, infarct_ctr, infarct_rad, infarct_BZ)
	write_infarct(extracted,0,"LV_infarct_BZ.txt")
	writeXMLUGrid(extracted,meshfilename_vtu)
	
	
	writer = vtk.vtkXMLUnstructuredGridWriter()
	writer.SetFileName(meshfilename_vtu)
	if(vtk.vtkVersion().GetVTKMajorVersion() > 5):
		writer.SetInputData(ugrid)
	else:
		writer.SetInput(ugrid)
	writer.Write()
	
	if(not is2ndorder):
	
		extract_surf = vtk.vtkGeometryFilter()
		if(vtk.vtkVersion().GetVTKMajorVersion() > 5):
			extract_surf.SetInputData(reader.GetOutput())
		else:
			extract_surf.SetInput(reader.GetOutput())
		extract_surf.Update()
		
		bds = extract_surf.GetOutput().GetBounds()
		C = [0,0, bds[5]-0.01]
		N = [0,0,1]
		
		isinsideout = True
		clipheart2 = clipheart(extract_surf.GetOutput(), C, N, isinsideout, verbose=True)
		epi, endo = splitDomainBetweenEndoAndEpi(clipheart2, verbose=True)
		if(endo.GetBounds()[1] - endo.GetBounds()[0] > epi.GetBounds()[1] - epi.GetBounds()[0]):
			temp = epi;
			epi = endo;
			endo = temp;
	
		reverse = vtk.vtkReverseSense()
		if(vtk.vtkVersion().GetVTKMajorVersion() < 6):
			reverse.SetInput(endo)
		else:
			reverse.SetInputData(endo)
		reverse.Update()
		endo.DeepCopy(reverse.GetOutput())
		
		#reverse = vtk.vtkReverseSense()
		#if(vtk.vtkVersion().GetVTKMajorVersion() < 6):
		#	reverse.SetInput(epi)
		#else:
		#	reverse.SetInputData(epi)
		#reverse.Update()
		#epi.DeepCopy(reverse.GetOutput())
	
		#writeSTL(endo, "endocardium.stl")
		#writeSTL(epi, "epicardium.stl")
		
		cmd = "rm " + meshfilename
		os.system(cmd)
	
		# add fiber
		endofilename = 'endocardium.stl'
		epifilename = 'epicardium.stl'
		endo_angle = 70
		epi_angle = -70
		ugrid = readXMLUGrid(meshfilename_vtu)
		pdata_endo = readSTL(endofilename)
		pdata_epi = readSTL(epifilename)
		addLocalProlateSpheroidalDirections(ugrid, pdata_endo, pdata_epi, type_of_support="cell")
		addLocalFiberOrientation(ugrid, endo_angle, epi_angle)
		cellcenter = getCellCenters(ugrid)
	
		writexmldatafile(cellcenter, fiberfilename_xml, "fiber vectors", cellcenter)
		writexmldatafile(cellcenter, sheetfilename_xml, "sheet vectors", cellcenter)
		writexmldatafile(cellcenter, sheetnormfilename_xml, "sheet normal vectors", cellcenter)
		
		writeXMLUGrid(ugrid, meshfilename_vtu)
		writeXMLPData(cellcenter, "fiberdirections.vtp")


#is2ndorder = False
infarct_ctr = [-1.0, 1.5, -5]
infarct_rad = 2
infarct_BZ = 4
#meshsize = 0.08; # Coarse Quad
#meshsize = 0.025; # Med
#meshsize = 0.030; # Med2
#meshsize = 0.02; # Med 2 Quad
#meshsize = 0.02; # Fine 
#meshsize = 0.05; # Fine 
#meshsize = 0.09; # Fine 

create_mesh(0.08, 'ellipsoidal')
#create_mesh(0.10, 'ellipsoidal')

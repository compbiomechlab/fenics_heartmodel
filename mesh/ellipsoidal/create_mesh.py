import vtk
import numpy as np
from vtk_py import *
import sys
import os
from gettetra import *
from writexmldatafile import *

def readMSHGrid(mshfilename):

	fdata = open(mshfilename, "r")
	lines = fdata.readlines()
	startelem = 0
	startnode = 0
	cnt = 0

	cellArray = vtk.vtkCellArray()
	points = vtk.vtkPoints()
	for line in lines:
		if("$Elements" in line):
			startelem = 1
			continue

		if("$EndElements" in line):
			startelem = 0
			continue

		if("$Nodes" in line):
			startnode = 1
			continue

		if("$EndNodes" in line):
			startnode = 0
			continue

		if(startnode == 1):
			try:
				x = float(line.strip().split()[1])
				y = float(line.strip().split()[2])
				z = float(line.strip().split()[3])

				points.InsertNextPoint(x, y, z)

			except IndexError:
				continue


		if(startelem == 1):
			try:
				elmtype = int(line.strip().split()[1])
				if(elmtype == 4):
					tetra = vtk.vtkTetra()
					tetra.GetPointIds().SetId(0, int(line.strip().split()[5])-1)
					tetra.GetPointIds().SetId(1, int(line.strip().split()[6])-1)
					tetra.GetPointIds().SetId(2, int(line.strip().split()[7])-1)
					tetra.GetPointIds().SetId(3, int(line.strip().split()[8])-1)
					cellArray.InsertNextCell(tetra)
			except IndexError:
				continue

	ugrid = vtk.vtkUnstructuredGrid()
	ugrid.SetPoints(points)
	ugrid.SetCells(10, cellArray)

	writeUGrid(ugrid, "test.vtk")
	
	return ugrid


def GetEpiEdge(ugrid, xmlfilename):

	extract_surf = vtk.vtkGeometryFilter()


	featureedge = vtk.vtkFeatureEdges()
	connectivity = vtk.vtkPolyDataConnectivityFilter()
	connectivity2 = vtk.vtkPolyDataConnectivityFilter()
	clean = vtk.vtkCleanPolyData()
	clean2 = vtk.vtkCleanPolyData()

	if(vtk.vtkVersion().GetVTKMajorVersion() > 5):
		extract_surf.SetInputData(ugrid)
		extract_surf.Update()

		for p in range(0, extract_surf.GetOutput().GetNumberOfCells()):
			bds = extract_surf.GetOutput().GetCell(p).GetBounds()
			if(abs(bds[4] - bds[5]) < 0.01):
				extract_surf.GetOutput().DeleteCell(p)

		extract_surf.GetOutput().RemoveDeletedCells()

		connectivity.SetInputData(extract_surf.GetOutput())
		connectivity.SetExtractionModeToLargestRegion()
		connectivity.Update()
		clean.SetInputData(connectivity.GetOutput())
		clean.Update()

		featureedge.SetInputData(clean.GetOutput())
		featureedge.Update()

		connectivity2.SetInputData(featureedge.GetOutput())
		connectivity2.SetExtractionModeToLargestRegion()
		connectivity2.Update()
		clean2.SetInputData(connectivity2.GetOutput())
		clean2.Update()
	
	else:
		extract_surf.SetInput(ugrid)
		extract_surf.Update()

		for p in range(0, extract_surf.GetOutput().GetNumberOfCells()):
			bds = extract_surf.GetOutput().GetCell(p).GetBounds()
			if(abs(bds[4] - bds[5]) < 0.01):
				extract_surf.GetOutput().DeleteCell(p)

		extract_surf.GetOutput().RemoveDeletedCells()

		connectivity.SetInput(extract_surf.GetOutput())
		connectivity.SetExtractionModeToLargestRegion()
		connectivity.Update()
		clean.SetInput(connectivity.GetOutput())
		clean.Update()

		featureedge.SetInput(clean.GetOutput())
		featureedge.Update()

		connectivity2.SetInput(featureedge.GetOutput())
		connectivity2.SetExtractionModeToLargestRegion()
		connectivity2.Update()
		clean2.SetInput(connectivity2.GetOutput())
		clean2.Update()

	kDTree = vtk.vtkKdTreePointLocator()
	kDTree.SetDataSet(ugrid)
	kDTree.BuildLocator()
	#kDTree.Update()

	fdata = open("edge_node_coordinates.txt", "w")
	Epi_node_no = vtk.vtkIdList()
	for p in range(0,clean2.GetOutput().GetNumberOfPoints()):
		pt = clean2.GetOutput().GetPoints().GetPoint(p)
		dist = vtk.mutable(0)
		vtkid = kDTree.FindClosestPoint([pt[0], pt[1], pt[2]])
		Epi_node_no.InsertNextId(vtkid)
		print >>fdata, ugrid.GetPoints().GetPoint(vtkid)[0], ugrid.GetPoints().GetPoint(vtkid)[1], ugrid.GetPoints().GetPoint(vtkid)[2]
		#print vtkid
	fdata.close()

	cells =  ugrid.GetCells()

	fdata = open(xmlfilename,"w")

	print >>fdata, "<?xml version='1.0'?>"
	print >>fdata, "<dolfin xmlns:dolfin='http://fenicsproject.org'>"
	#print >>fdata, "<mesh_function>"
	print >>fdata, "<mesh_value_collection name='f' type='uint' dim='1' size='%s'>"%(ugrid.GetNumberOfCells()*6)
	#print >>fdata, "<mesh_function type='uint' dim='1' size='%s'>"%(ugrid.GetNumberOfCells()*6)


	cnt = 0
	for cellno in range(0,ugrid.GetNumberOfCells()):

		pts = vtk.vtkIdList()
		ugrid.GetCellPoints(cellno, pts)

		if((Epi_node_no.IsId(pts.GetId(0)) != -1) and (Epi_node_no.IsId(pts.GetId(1)) != -1)):
			print cellno
			print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,4,10) 
			#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,10) 
			cnt = cnt + 1
		#else:
		#	print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,0,0) 
		#	#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,0) 
		#	cnt = cnt + 1

		if((Epi_node_no.IsId(pts.GetId(1)) != -1) and (Epi_node_no.IsId(pts.GetId(2)) != -1)):
			print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,5,10) 
			#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,10) 
			cnt = cnt + 1
		#else:
		#	print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,4,0) 
		#	#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,0) 
		#	cnt = cnt + 1


		if((Epi_node_no.IsId(pts.GetId(0)) != -1) and (Epi_node_no.IsId(pts.GetId(3)) != -1)):
			print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,1,10) 
			#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,10) 
			cnt = cnt + 1

		#else:
		#	print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,1,0) 
		#	#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,0) 
		#	cnt = cnt + 1

		if((Epi_node_no.IsId(pts.GetId(0)) != -1) and (Epi_node_no.IsId(pts.GetId(2)) != -1)):
			print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,2,10) 
			#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,10) 
			cnt = cnt + 1
		#else:
		#	print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,3,0) 
		#	#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,0) 
		#	cnt = cnt + 1

		if((Epi_node_no.IsId(pts.GetId(1)) != -1) and (Epi_node_no.IsId(pts.GetId(3)) != -1)):
			print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,3,10) 
			#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,10) 
			cnt = cnt + 1
		#else:
		#	print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,2,0) 
		#	#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,0) 
		#	cnt = cnt + 1

		if((Epi_node_no.IsId(pts.GetId(2)) != -1) and (Epi_node_no.IsId(pts.GetId(3)) != -1)):
			print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,0,10) 
			#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,10) 
			cnt = cnt + 1
		#else:
		#	print >>fdata, "<value cell_index='%s' local_entity='%s' value='%s' />"%(cellno,5,0) 
		#	#print >>fdata, "<entity index='%s' value='%s' />"%(cnt,0) 
		#	cnt = cnt + 1


		
	print >>fdata, "</mesh_value_collection>"
	#print >>fdata, "</mesh_function>"
	print >>fdata, "</dolfin>"


	

def create_mesh(meshfilename, meshsize, isshrink):

	# add fiber
	meshfilename_vtk = meshfilename + ".vtk"
	meshfilename_vtu = meshfilename + ".vtu"
	meshfilename_msh = meshfilename + ".msh"
	fiberfilename_xml = meshfilename + "_fiber.xml"
	sheetfilename_xml = meshfilename + "_sheet.xml"
	sheetnormfilename_xml = meshfilename + "_sheetnorm.xml"
	edgefilename_xml = meshfilename + "_edge.xml"
	#
	os.system('cp ellipsoidal_freecad.geo ellipsoidal_freecad_temp.geo')
	cmd = "sed -i 's/<<Meshsize>>/"+"%5.3f"%(meshsize)+"/g' ellipsoidal_freecad_temp.geo"
	os.system(cmd)
	cmd = "gmsh -3 ellipsoidal_freecad_temp.geo -o %s"%(meshfilename_vtk)
	os.system(cmd)
	cmd = "gmsh -3 ellipsoidal_freecad_temp.geo -o %s"%(meshfilename_msh)
	os.system(cmd)
	cmd = "rm ellipsoidal_freecad_temp.geo"
	#os.system(cmd)

	## Convert vtk to vtu file
	#reader = vtk.vtkUnstructuredGridReader()
	#reader.SetFileName(meshfilename_vtk)
	#reader.Update()

	## Get tetrahedron
	#ugrid = reader.GetOutput();
	#ugrid = gettetra(ugrid)

	ugrid = readMSHGrid(meshfilename_msh)

	writeXMLUGrid(ugrid, "test.vtu")
	stop


	if(isshrink):
	  transform = vtk.vtkTransform();
  	  transform.Scale(0.1,0.1,0.1);
 
          transformFilter = vtk.vtkTransformFilter();
	  if(vtk.vtkVersion().GetVTKMajorVersion() > 5):
  	  	transformFilter.SetInputData(ugrid);
	  else:
  	  	transformFilter.SetInput(ugrid);
  	  transformFilter.SetTransform(transform);
	  ugrid = transformFilter.GetOutput()
		
	writer = vtk.vtkXMLUnstructuredGridWriter()
	writer.SetFileName(meshfilename_vtu)
	if(vtk.vtkVersion().GetVTKMajorVersion() > 5):
		writer.SetInputData(ugrid)
	else:
		writer.SetInput(ugrid)
	writer.Write()
	
	# Set Find endo and epi	
	extract_surf = vtk.vtkGeometryFilter()
	if(vtk.vtkVersion().GetVTKMajorVersion() > 5):
		extract_surf.SetInputData(ugrid)
	else:
		extract_surf.SetInput(ugrid)
	extract_surf.Update()
	
	bds = extract_surf.GetOutput().GetBounds()
	C = [0,0, bds[5]-0.01]
	N = [0,0,1]
	
	isinsideout = True
	clipheart_ = clipheart(extract_surf.GetOutput(), C, N, isinsideout, verbose=True)
	epi, endo = splitDomainBetweenEndoAndEpi(clipheart_, verbose=True)

	if(endo.GetBounds()[1] - endo.GetBounds()[0] > epi.GetBounds()[1] - epi.GetBounds()[0]):
		temp = epi;
		epi = endo;
		endo = temp;
	
	#reverse = vtk.vtkReverseSense()
	#if(vtk.vtkVersion().GetVTKMajorVersion() < 6):
	#	reverse.SetInput(endo)
	#else:
	#	reverse.SetInputData(endo)
	#reverse.Update()
	#endo.DeepCopy(reverse.GetOutput())
	
	#reverse = vtk.vtkReverseSense()
	#if(vtk.vtkVersion().GetVTKMajorVersion() < 6):
	#	reverse.SetInput(epi)
	#else:
	#	reverse.SetInputData(epi)
	#reverse.Update()
	#epi.DeepCopy(reverse.GetOutput())
	
	writeSTL(endo, "endocardium.stl")
	writeSTL(epi, "epicardium.stl")
	
	cmd = "rm " + meshfilename_vtk
	os.system(cmd)
	
	# add fiber
	endofilename = 'endocardium.stl'
	epifilename = 'epicardium.stl'
	endo_angle = 60
	epi_angle = -60
	#ugrid = readXMLUGrid(meshfilename_vtu)
	pdata_endo = readSTL(endofilename)
	pdata_epi = readSTL(epifilename)
	addLocalProlateSpheroidalDirections(ugrid, pdata_endo, pdata_epi, type_of_support="cell")
	addLocalFiberOrientation(ugrid, endo_angle, epi_angle)
	cellcenter = getCellCenters(ugrid)
	
	writexmldatafile(cellcenter, fiberfilename_xml,  "fiber vectors")
	writexmldatafile(cellcenter,  sheetfilename_xml, "sheet vectors")
	writexmldatafile(cellcenter,  sheetnormfilename_xml, "sheet normal vectors")
	
	writeXMLUGrid(ugrid, meshfilename_vtu)
	writeXMLPData(cellcenter, "fiberdirections.vtp")

	# get Epicardial edge
	#ugrid = readMSHGrid("MIPat05.msh")
	GetEpiEdge(ugrid, edgefilename_xml)

#create_mesh("ellipsoidal_fc_fine", 0.10, False)
#create_mesh("ellipsoidal_fc", 0.70, False)
create_mesh("ellipsoidal_fc_coarse", 0.6, False)

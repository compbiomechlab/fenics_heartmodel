Geometry.HideCompounds = 0;

//For speeding up the mesh calculations
//To avoid a too high mesh density  inside the cylinder

Mesh.CharacteristicLengthFactor = <<Meshsize>> ;
lc = <<Meshsize>>;

Mesh.Algorithm    = 2; // (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad) (Default=2)
Mesh.RecombineAll = 0;
Mesh.RemeshAlgorithm = 1; // (0=no split, 1=automatic, 2=automatic only with metis) (Default=0)
Mesh.RemeshParametrization = 7; // (0=harmonic_circle, 1=conformal_spectral, 2=rbf, 3=harmonic_plane, 4=convex_circle, 5=convex_plane, 6=harmonic square, 7=conformal_fe) (Default=4)

Mesh.Algorithm3D    = 4; // (1=Delaunay, 4=Frontal, 5=Frontal Delaunay, 6=Frontal Hex, 7=MMG3D, 9=R-tree) (Default=1)
Mesh.Recombine3DAll = 1;
Mesh.Optimize = 1;

Merge "ellipsoid.step";
//CreateTopology;
//
//Lcmin = 2.*lc / 8.
//
//Point(10000) = {0,0,-8.3,0.01};
//Field[1] = Attractor;
//Field[1].NodesList = {10000};
//
//Field[2] = Threshold;
//Field[2].IField = 1;
//Field[2].LcMin = 0.01;
//Field[2].LcMax =  0.5;
//Field[2].DistMin = 1.0;
//Field[2].DistMax = 5.0;
//
//// Use minimum of all the fields as the background field
//Field[3] = Min;
//Field[3].FieldsList = {2};
//Background Field = 3;

Volume(1) = {1};

Physical Volume(1) = 1;
Physical Surface(1) = 1;
Physical Surface(2) = 2;
Physical Surface(3) = 3;

//Mesh.CharacteristicLengthExtendFromBoundary = 0;


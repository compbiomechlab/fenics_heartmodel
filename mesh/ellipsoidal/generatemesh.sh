#!/bin/sh

#python create_ellipsoidal.py
#dolfin-convert ellipsoidal.msh ellipsoidal.xml
#dolfin-convert ellipsoidal_fine.msh ellipsoidal_fine.xml

python create_mesh.py
#dolfin-convert ellipsoidal_fc.msh ellipsoidal_fc.xml
dolfin-convert ellipsoidal_fc_coarse.msh ellipsoidal_fc_coarse.xml
#dolfin-convert ellipsoidal_fc_fine.msh ellipsoidal_fc_fine.xml

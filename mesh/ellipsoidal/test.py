from dolfin import *

mesh = Mesh("ellipsoidal_fc_coarse.xml")
facetboundaries = MeshFunction("size_t", mesh, "ellipsoidal_fc_coarse_facet_region.xml")

plot(facetboundaries, interactive=True)

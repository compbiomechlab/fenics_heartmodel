import vtk

def writexmldatafile(data, filename, fieldname):


	fiberfile = open(filename, "w")
	
	print >>fiberfile, "<?xml version='1.0'?>"
	print >>fiberfile, "<dolfin xmlns:dolfin='http://fenicsproject.org'>"
	print >>fiberfile, "<function_data size='%s'>"%(data.GetNumberOfPoints()*3)

	for p in range(0, data.GetNumberOfPoints()):
		print >>fiberfile, "<dof index='%s' value='%s' cell_index='%s' cell_dof_index='%s' />"%(p*3,\
		                   data.GetPointData().GetArray(fieldname).GetTuple3(p)[0],p,0) 
		print >>fiberfile, "<dof index='%s' value='%s' cell_index='%s' cell_dof_index='%s' />"%(p*3+1,\
		                   data.GetPointData().GetArray(fieldname).GetTuple3(p)[1],p,1) 
		print >>fiberfile, "<dof index='%s' value='%s' cell_index='%s' cell_dof_index='%s' />"%(p*3+2,\
		                   data.GetPointData().GetArray(fieldname).GetTuple3(p)[2],p,2) 

	print >>fiberfile, "</function_data>"
	print >>fiberfile, "</dolfin>"



from vtk_py import *

def gettetra(mesh):

	cta = vtk.vtkIntArray()
	cta.SetName("cell types")
	cta.SetNumberOfComponents(1)
	cta.SetNumberOfTuples(mesh.GetNumberOfCells())
	for x in xrange(0,mesh.GetNumberOfCells()):
	  cta.SetValue(x,mesh.GetCell(x).GetCellType())

	mesh.GetCellData().AddArray(cta)

	threshold = vtk.vtkThreshold()
	threshold.ThresholdBetween(9,11)
	if(vtk.vtkVersion.GetVTKMajorVersion() < 6):
		threshold.SetInput(mesh)
	else:
		threshold.SetInputData(mesh)
	threshold.SetInputArrayToProcess(0, 0, 0, 1, "cell types");
  	threshold.Update();

	return threshold.GetOutput()



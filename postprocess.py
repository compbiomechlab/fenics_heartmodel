from dolfin import *
import solver as solver

def ActiveStressArts(w, Tmax, isvolctrl, f0, s0, n0):

	# ARTS MODEL #########################################################################################################################
	Crest = Constant(0.00)
	Lsc0 = Constant(1.51)
	Lsref = Constant(2.0)
	Lseiso = Constant(0.04)
	vmax = Constant(7.0e-3)
	tauD = Constant(32.0) # 32.0 # 100.0 # 32.0 # 33.8 #  
	tauR = Constant(48.0) # 28.1 #48.0 #28.1*3 # 48.0 # 28.1 #  
	tauSC = Constant(425.0) # 250.0 #  425.0
	bh = Constant(1.5)
	ah = Constant(1.5)

	if(isvolctrl):
		(u,p,Theta,Carts,Larts,Pendo) = split(w);
	else:
		(u,p,Theta,Carts,Larts) = split(w);

	Ls = computeFiberStretch(u, f0)*Lsref

	Tact =  Tmax  * Carts * (Larts - Lsc0) * (Ls -  Larts) / Lseiso #* p1

	return Tact

def computeCauchyStretch(u):

	d = u.geometric_dimension()
	I = Identity(d)             # Identity tensor
	F = I + grad(u)             # Deformation gradient
	Cmat = F.T*F       	    # Right Cauchy-Green tensor

	return Cmat

def computeFiberStretch(u, f0):

	Cmat = computeCauchyStretch(u)
	Ls = sqrt(dot(f0, Cmat*f0))

	return Ls



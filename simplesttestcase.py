from dolfin import *
import numpy as np
import math as math
import postprocess as postprocess
from updateGuccioneVariable import *
import utilities as util


quad_degree = 4

# Optimization options for the form compiler
parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
ffc_options = {"optimize": True, \
               "eliminate_zeros": True, \
               "precompute_basis_const": True, \
               "precompute_ip_const": True}
dolfin.parameters["form_compiler"]["quadrature_degree"] = quad_degree

# Create mesh and define function space
L = 10.0
mesh = BoxMesh(Point(0.0, 0.0, 0.0), Point(L, 1.0, 1.0), 10, 2, 2)
N = FacetNormal ( mesh )
dx = dolfin.dx(mesh, metadata = {"integration_order":quad_degree})

# Mark boundary subdomians
left =  CompiledSubDomain("near(x[0], side) && on_boundary", side = 0.0)
right = CompiledSubDomain("near(x[0], side) && on_boundary", side = L)
top = CompiledSubDomain("near(x[2], side) && on_boundary", side = 1.0)
bot = CompiledSubDomain("near(x[2], side) && on_boundary", side = 0.0)
front = CompiledSubDomain("near(x[1], side) && on_boundary", side = 1.0)
back = CompiledSubDomain("near(x[1], side) && on_boundary", side = 0.0)

facetboundaries = FacetFunction("size_t", mesh)     
facetboundaries.set_all(0) 
right.mark(facetboundaries, 2)
left.mark(facetboundaries, 1)
top.mark(facetboundaries, 3)
front.mark(facetboundaries, 4)
ds = Measure("ds")[facetboundaries]  

W = VectorFunctionSpace(mesh, "Lagrange", 2)
if(dolfin.dolfin_version() == '1.7.0dev'):
	Quadscalar = FiniteElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme='default')
	Quads = FunctionSpace(mesh, Quadscalar)
else:
	Quads = FunctionSpace(mesh, "Quadrature", quad_degree)

bcleft = DirichletBC(W, Constant(("0","0","0")), facetboundaries, 1)
bctop = DirichletBC(W.sub(2), Constant(("0")), facetboundaries, 3)
bcfront = DirichletBC(W.sub(1), Constant(("0")), facetboundaries, 4)
bcright = DirichletBC(W, Constant(("0","0","0")), facetboundaries, 2)

bcs = [bcleft, bctop, bcfront]
bcs2 = [bcleft, bctop, bcfront, bcright]

# Define functions
du = TrialFunction(W)            # Incremental displacement
v  = TestFunction(W)             # Test function
u  = Function(W)                 # Displacement from previous iteration
Tact = Function(Quads)
Lart = Function(Quads)
Cart = Function(Quads)

d = u.geometric_dimension()
I = Identity(d)             
F = I + grad(u)             
J = det(F)
Cmat = F.T*F       	    
Fdev = J**(-1.0/3.0)*F
Cdev = Fdev.T*Fdev
Kappa = Constant(1.0e5);
n = cofac(F)*N
f0 = Constant(("1.0", "0.0", "0.0"))
s0 = Constant(("0.0", "1.0", "0.0"))
n0 = Constant(("0.0", "0.0", "1.0"))
Press = Expression(("P"), P=0.0)

Ccoeff = Constant(2000);
bf = Constant(18.48);
bfx = Constant(3.58);
bxx = Constant(1.627);
Edev = Constant(0.5)*(Cdev - I)
Qmat = util.Qtransform(f0, s0, n0)
Edevloc = (Qmat)*Edev*(Qmat.T)

QQ = bf*Edevloc[0,0]**2 + bxx*(Edevloc[1,1]**2 + Edevloc[2,2]**2 + Edevloc[1,2]**2 + Edevloc[2,1]**2) + bfx*(Edevloc[0,1]**2 + Edevloc[1,0]**2 + Edevloc[0,2]**2 + Edevloc[0,2]**2)
	
#W1 = Constant(1000.0)*(tr(Cdev) - Constant(3.0)) + Kappa/Constant(2.0)*(J - 1.0)**2.0
W1 = Ccoeff*(exp(QQ) - Constant(1.0)) + Kappa/Constant(2.0)*(J - 1.0)**2.0
W2 = Tact/2.0 * (Cmat[0,0] - 1.0)
Wtotal = (W1 + W2)

# Total potential energy
Pi = Wtotal*dx  + inner(u, -1.0*Press*n)*ds(2)

# Compute first variation of Pi (directional derivative about u in the direction of v)
F = derivative(Pi, u, v)

# Compute Jacobian of F
Jac = derivative(F, u, du)

t_niter = 40
r_threshold = 1e-9

# Solve variational problem
dt = 2.0
t = 0.0
Tmax = 80e3
Tend = 800.0;
ngpt = len(Tact.vector().array()[:])
t_niter = 40
r_threshold = 1e-9

# Varying the inhomogenity of the stiffness
disp_vtk_file = File("nonlinearelasticity_block_disp_diag.pvd")
Tmax = 0.0001
for p in range(0, 5):
	Press.P += 500.00

	print "Load step = ", p
	solve(F == 0, u, bcs, J=Jac,form_compiler_parameters=ffc_options)
	disp_vtk_file << (u, p*0.01)


Tmax = 80e3
fdata = open("Tact.txt","w")
while(t < Tend):

	dt = 1.0

	# Modifying stiffness at the Gauss points
	Cart_gpt = Cart.vector().array()[:]
	Lart_gpt = Lart.vector().array()[:]
	Tact_gpt = Tact.vector().array()[:]

	lbda = postprocess.computeFiberStretch(u, f0)
	lbda_gpt = project(lbda, Quads).vector().array()[:]

	Cart_gpt, Lart_gpt, Tact_gpt = updateGuccioneVariable(Tmax, Lart_gpt, Cart_gpt, dt, lbda_gpt, t)

	#Tact.vector()[:] = np.ones(ngpt)*sin(t/Tend*math.pi)*80e3 + np.random.rand(1,ngpt)[0]*stiffness_diff
	print "Max stiffness = ", max(Tact.vector().array()),  " Min stiffness = ", min(Tact.vector().array())
	print lbda_gpt
	print Tact_gpt
	print >>fdata, t, Tact_gpt[0], lbda_gpt[0], Cart_gpt[0], Lart_gpt[0], Tact_gpt[0]	

	Cart.vector()[:] = Cart_gpt
	Lart.vector()[:] = Lart_gpt
	Tact.vector()[:] = Tact_gpt

	solve(F == 0, u, bcs2, J=Jac,form_compiler_parameters=ffc_options)

	t = t + dt
	disp_vtk_file << (u, t)

# Plot and hold solution
plot(u, interactive=True, mode="displacement")


from dolfin import *
from numpy import *
import solver as solver

quad_degree = 2
dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3'
dolfin.parameters["form_compiler"]["cpp_optimize"] = True
dolfin.parameters["form_compiler"]["optimize"] = True
#dolfin.parameters["form_compiler"]["representation"] = "uflacs"
#dolfin.parameters['allow_extrapolation'] = True

L = 10.0
mesh = BoxMesh(Point(0.0, 0.0, 0.0), Point(L, 1.0, 1.0), 1, 1, 1)

# Mark boundary subdomians
left =  CompiledSubDomain("near(x[0], side) && on_boundary", side = 0.0)
right = CompiledSubDomain("near(x[0], side) && on_boundary", side = L)
top = CompiledSubDomain("near(x[2], side) && on_boundary", side = 1.0)
bot = CompiledSubDomain("near(x[2], side) && on_boundary", side = 0.0)
front = CompiledSubDomain("near(x[1], side) && on_boundary", side = 1.0)
back = CompiledSubDomain("near(x[1], side) && on_boundary", side = 0.0)

# Define essential boundary
facetboundaries = FacetFunction("size_t", mesh)     
facetboundaries.set_all(0) 
right.mark(facetboundaries, 2)
left.mark(facetboundaries, 1)
top.mark(facetboundaries, 3)
front.mark(facetboundaries, 4)

W = VectorFunctionSpace(mesh, "Lagrange", 2)
print dolfin.dolfin_version() 
if(dolfin.dolfin_version() == '1.7.0dev'):
	Quadscalar = FiniteElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme='default')
	Quads = FunctionSpace(mesh, Quadscalar)
else:
	Quads = FunctionSpace(mesh, "Quadrature", quad_degree)

bcright = DirichletBC(W.sub(0), Constant(("0")), facetboundaries, 2)
bctop = DirichletBC(W.sub(2), Constant(("0")), facetboundaries, 3)
bcfront = DirichletBC(W.sub(1), Constant(("0")), facetboundaries, 4)
bcleft = DirichletBC(W.sub(0), Constant(("0")), facetboundaries, 1)

bcs = [bcleft, bctop, bcfront]#, bcright]

dx1 = dolfin.dx(mesh, metadata = {"integration_order":quad_degree})

u = Function(W)
v = TestFunction(W)
du = TrialFunction(W)

Tact = Function(Quads)

d = u.geometric_dimension()
I = Identity(d)             
F = I + grad(u)             
J = det(F)
Cmat = F.T*F       	    
Fdev = J**(-1.0/3.0)*F
Cdev = Fdev.T*Fdev
Kappa = Constant(1.0e3);
f0 = Constant(("1.0", "0.0", "0.0"))

W1 = Constant(1000.0)*(tr(Cdev) - Constant(3.0)) + Kappa/Constant(2.0)*(J - 1.0)**2.0
W2 = Tact/2.0 * (Cmat[0,0] - 1.0)
Wtotal = (W1 + W2)*dx1

u = Function(W)

L = derivative(Wtotal, u, v)
Jac = derivative(L, u, du)

#problem = NonlinearVariationalProblem(L, u, bcs, Jac)
#solver  = NonlinearVariationalSolver(problem)

dt = 1.0
t = 0.0
Tend = 100.0;
ngpt = len(Tact.vector().array()[:])
t_niter = 40
r_threshold = 1e-9



disp_vtk_file = File("nonlinearelasticity_block_disp_diag.pvd")
while(t < Tend):

	Tact.vector()[:] = ones(ngpt)*t*100 + random.rand(1,ngpt)[0]*10
	#print Tact.vector().array()[:]

	u, nIter, eps = solver.solveLVP(u, W, bcs, Jac, L, t_niter, r_threshold)

	t = t + dt

	disp_vtk_file << (u, t)


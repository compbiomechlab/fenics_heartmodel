from dolfin import *
import numpy as np
import math as math

def solveLVP(w, W, bcs, Jac, L, t_niter, r_threshold):#, dofs_theta):

    bcs_du = bcs
    w_inc = Function(W)

    nIter = 0
    eps = 1

    #disp_diag_vtk_file = File("nonlinearelasticity_disp_diag.pvd")
    #Lbda_diag_vtk_file = File("nonlinearelasticity_Lbda_diag.pvd")
    #Carts_diag_vtk_file = File("nonlinearelasticity_Carts_diag.pvd")
    #Larts_diag_vtk_file = File("nonlinearelasticity_Larts_diag.pvd")

    b = None
    while eps > r_threshold and nIter < t_niter:              # Newton iterations
        nIter += 1
   	A = assemble(Jac)
	b = assemble(-L, tensor=b)
	for bc in bcs_du:
		bc.apply(A, b)

	
        solve(A, w_inc.vector(), b, "mumps")
        eps = np.linalg.norm(w_inc.vector().array(), ord = 2)

        fnorm = b.norm('l2')
        lmbda = 1.0     

        w.vector()[:] += lmbda*w_inc.vector()    # New u vector
        print '      {0:2d}  {1:3.2E}  {2:5e}'.format(nIter, eps, fnorm)


	#Crest = Constant(0.00)
	#Lsc0 = Constant(1.51)
	#Lsref = Constant(2.0)
	#Lseiso = Constant(0.04)

	#Lart_gpt = project(w.sub(4), Quads).vector()[:]
	#Cart_gpt = project(w.sub(3), Quads).vector()[:]
	#F = I + grad(w.sub(0))
	#Lbda = sqrt(dot(f0, (F.T*F)*f0))  
	#Ls = sqrt(dot(f0, (F.T*F)*f0))*Lsref
	#Ls_gpt = project(Ls,Quads).vector()[:]
	#Lbda_gpt = project(Lbda,Quads).vector()[:]
	#u_gpt = project(w.sub(0), Quad).vector()[:]

	#p1 = conditional(gt(Ls,w.sub(4)), 1.0, -0.1)
	#Tact =  Tmax  * w.sub(3) * (w.sub(4) - Lsc0) * (Lbda*Lsref -  w.sub(4)) / Lseiso #* p1
	#Tact_gpt = project(Tact,Quads).vector()[:]
	#p1_gpt = project(p1, Quads).vector()[:]

	#print "min Lart = ", min(Lart_gpt), " max Lart = ", max(Lart_gpt)
	#print "min Cart = ", min(Cart_gpt), " max Cart = ",  max(Cart_gpt)
	#print "min Lbda = ", min(Lbda_gpt), " max Ls = ", max(Lbda_gpt)
	#print "min Ls = ", min(Ls_gpt), " max Ls = ", max(Ls_gpt)
	#print "min Ls - Lart = ", min(Ls_gpt - Lart_gpt), " max Ls - Lart = ",  max(Ls_gpt - Lart_gpt)
	#print "min Tact = ", min(Tact_gpt), " max Tact = ",  max(Tact_gpt)
	#print "min p1 = ", min(p1_gpt), " max p1 = ",  max(p1_gpt)
	#print "\n"

	#disp_diag_vtk_file << (w.sub(0), float(nIter))

	if(math.isnan(eps)):
		break;

    #LVcavvol = cavityvol(n,w,X, endoid)
    #LVwallvol = wallvol(w,X)
    #LVP = cavitypressure(W, w)

    return w, nIter, eps #LVP, LVwallvol, LVcavvol, nIter, eps



def solveLVP2(w, volume, bcs, Jac, L, endoid, t_niter, r_threshold):#, dofs_theta):

    V0.vol = volume

    bcs_du = bcs
    w_inc = Function(W)

    nIter = 0
    eps = 1

    disp_diag_vtk_file = File("nonlinearelasticity_disp_diag.pvd")
    Lbda_diag_vtk_file = File("nonlinearelasticity_Lbda_diag.pvd")
    Carts_diag_vtk_file = File("nonlinearelasticity_Carts_diag.pvd")
    Larts_diag_vtk_file = File("nonlinearelasticity_Larts_diag.pvd")

    b = None
    while eps > r_threshold and nIter < t_niter:              # Newton iterations
        nIter += 1
   	A = assemble(Jac)
	b = assemble(-L, tensor=b)
	for bc in bcs_du:
		bc.apply(A, b)
        solve(A, w_inc.vector(), b, "mumps")
        eps = np.linalg.norm(w_inc.vector().array(), ord = 2)

        fnorm = b.norm('l2')
        lmbda = 1.0     

        w.vector()[:] += lmbda*w_inc.vector()    # New u vector
        print '      {0:2d}  {1:3.2E}  {2:5e}'.format(nIter, eps, fnorm)

	Crest = Constant(0.00)
	Lsc0 = Constant(1.51)
	Lsref = Constant(2.0)
	Lseiso = Constant(0.04)

	Lart_gpt = project(w.sub(4), Quads).vector()[:]
	Cart_gpt = project(w.sub(3), Quads).vector()[:]
	F = I + grad(w.sub(0))
	Lbda = sqrt(dot(f0, (F.T*F)*f0))  
	Ls = sqrt(dot(f0, (F.T*F)*f0))*Lsref
	Ls_gpt = project(Ls,Quads).vector()[:]
	Lbda_gpt = project(Lbda,Quads).vector()[:]
	#u_gpt = project(w.sub(0), Quad).vector()[:]

	p1 = conditional(gt(Ls,w.sub(4)), 1.0, -0.1)
	Tact =  Tmax  * w.sub(3) * (w.sub(4) - Lsc0) * (Lbda*Lsref -  w.sub(4)) / Lseiso #* p1
	Tact_gpt = project(Tact,Quads).vector()[:]
	p1_gpt = project(p1, Quads).vector()[:]

	#print "min Lart = ", min(Lart_gpt), " max Lart = ", max(Lart_gpt)
	#print "min Cart = ", min(Cart_gpt), " max Cart = ",  max(Cart_gpt)
	#print "min Lbda = ", min(Lbda_gpt), " max Ls = ", max(Lbda_gpt)
	#print "min Ls = ", min(Ls_gpt), " max Ls = ", max(Ls_gpt)
	#print "min Ls - Lart = ", min(Ls_gpt - Lart_gpt), " max Ls - Lart = ",  max(Ls_gpt - Lart_gpt)
	#print "min Tact = ", min(Tact_gpt), " max Tact = ",  max(Tact_gpt)
	#print "min p1 = ", min(p1_gpt), " max p1 = ",  max(p1_gpt)
	#print "\n"

	disp_diag_vtk_file << (w.sub(0), float(nIter))

	if(math.isnan(eps)):
		break;

    #LVcavvol = cavityvol(n,w,X, endoid)
    #LVwallvol = wallvol(w,X)
    #LVP = cavitypressure(W, w)

    return w, nIter, eps #LVP, LVwallvol, LVcavvol, nIter, eps



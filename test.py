from dolfin import *
import numpy as np
from updateArtsVariable import *
from matplotlib import pylab as plt

# Create mesh and define function space
quad_degree = 4
mesh = UnitSquareMesh(40, 40)
V = FunctionSpace(mesh, "Lagrange", 1)
Q = FunctionSpace(mesh, "Quadrature", quad_degree)

# Define Dirichlet boundary (x = 0 or x = 1)
def boundary(x):
    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, boundary)

Cart_gpt = Function(Q)
Lart_gpt = Function(Q)
Tact_gpt = Function(Q)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("10*exp(-(pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2)) / 0.02)")
g = Expression("sin(5*x[0])")
K = Function(Q)
a = inner(grad(u), (Tact_gpt+Constant(1.0))*grad(v))*dx
L = f*v*dx + g*v*ds

t = 0;
dt = 2;
T = 800;
Tmax = 1.0;#80e3

ngpt = len(Lart_gpt.vector().array())
lbda = np.ones(ngpt)

Tact_stored = []
Cart_stored = []
Lart_stored = []
t_stored = []

u = Function(V)
file = File("poisson.pvd")

while(t < T):
	
	print "t = ", t
	Cart = Cart_gpt.vector().array()[:]
	Lart = Lart_gpt.vector().array()[:]
	Tact = Tact_gpt.vector().array()[:]

	Cart, Lart, Tact = updateArtsVariable(Tmax, Lart, Cart, dt, lbda, t)

	Cart_gpt.vector()[:] = Cart
	Lart_gpt.vector()[:] = Lart
	Tact_gpt.vector()[:] = Tact

	Tact_stored.append(Tact)
	Cart_stored.append(Cart)
	Lart_stored.append(Lart)
	t_stored.append(t)

	t += dt

	solve(a == L, u, bc)
	file << u



#plt.plot(t_stored, Tact_stored, '--')
#plt.show()





# Define boundary condition
#u0 = Constant(0.0)
#bc = DirichletBC(V, u0, boundary)
#
## Define variational problem
#u = TrialFunction(V)
#v = TestFunction(V)
#f = Expression("10*exp(-(pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2)) / 0.02)")
#g = Expression("sin(5*x[0])")
#K = Function(Q)
#a = inner(grad(u), K*grad(v))*dx
#L = f*v*dx + g*v*ds
#
## Compute solution
#u = Function(V)
#
#ngpt = len(K.vector().array())
#print ngpt
#K.vector()[:] = np.ones(ngpt)*10
#solve(a == L, u, bc)

# Plot solution
#plot(u, interactive=True)




from updateGuccioneVariable import *
import numpy as np
from matplotlib import pylab as plt

lbda = np.array([0.8, 1.5])
C = np.array([1.0, 1.0])
L = np.array([1.0, 1.0])

Tmax = 80e3
dt = 5 
t_a = 0.

t_a_array = []
Tact_array = []

while (t_a < 800):
	t_a = t_a + dt

	Cnew,Lnew,Tact = updateGuccioneVariable(Tmax, L, C, dt, lbda, t_a)
	Tact_array.append(Tact)
	t_a_array.append(t_a)

plt.plot(t_a_array, Tact_array)
plt.show()




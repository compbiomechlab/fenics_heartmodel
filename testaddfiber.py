from dolfin import *
from addfiber import *
import vtk as vtk

quad_degree = 4
isvolctrl = False 


dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3'
dolfin.parameters["form_compiler"]["cpp_optimize"] = True
dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["quadrature_degree"] = quad_degree
dolfin.parameters["form_compiler"]["representation"] = "uflacs"
dolfin.parameters['allow_extrapolation'] = True

# Geometry ------------------------------------------------------------------
topid = 2
endoid = 3
epiid = 1
casename = "../mesh/ellipsoidal/ellipsoidal_fc_coarse"
fibercasename = "../mesh/ellipsoidal/ellipsoidal_fc"
meshfilename = casename + ".xml"
facetfilename = casename + "_facet_region.xml"
subdomainfilename = casename + "_physical_region.xml"
mesh = Mesh(meshfilename)
N = FacetNormal ( mesh )
facetboundaries = MeshFunction('size_t', mesh, facetfilename)
subdomains = MeshFunction("size_t", mesh, subdomainfilename)
rfun = MeshFunction("size_t", mesh, 1)
rfun.set_all(0)
bl = CompiledSubDomain("(x[1]*x[1] + x[0]*x[0]) > 3.5*3.5 - 1.0 && x[2]*x[2] < 0.1")
bl.mark(rfun, 10)

V = VectorFunctionSpace(mesh, "Lagrange", 2)
Q = FunctionSpace(mesh, "Lagrange",1)
DG = FunctionSpace(mesh, "DG",0)
Pspace = FunctionSpace(mesh, "Real", 0) 


if(isvolctrl):
	W = MixedFunctionSpace([V,DG,Q,Q,Q,Pspace])
else: 
	W = MixedFunctionSpace([V,DG,Q,Q,Q])

if(dolfin.dolfin_version() == '1.7.0dev'):
	print "Dolfin 1.7.0dev"
	Quadelem = VectorElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, dim=3, quad_scheme='default')
	Quad = FunctionSpace(mesh, Quadelem)

	Quadscalar = FiniteElement("Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme='default')
	Quads = FunctionSpace(mesh, Quadscalar)

else:
	print "Dolfin 1.6"
	Quad = VectorFunctionSpace(mesh, "Quadrature", quad_degree)
	Quads = FunctionSpace(mesh, "Quadrature", quad_degree)


f0, s0, n0 = addfiber(mesh, Quad, fibercasename)




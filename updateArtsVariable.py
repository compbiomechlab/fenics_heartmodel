import numpy as np

def updateArtsVariable(Tmax, L, C, dt, lbda, t_a):

	Crest = 0.04
	Lsc0 = 1.51
	Lsref = 2.0
	Lseiso = 0.04
	vmax = 7.0e-3
	tauD = 32.0
	tauR = 48.0
	tauSC = 425.0

	Ls = lbda*Lsref	

	Lnew =  L + dt*(Ls/Lseiso - 1.0)*vmax
	Lnew = Lnew /(1.0 + dt*vmax/Lseiso)

	TLsc = tauSC*(0.29 + 0.3*(Lnew))
	CLsc = np.tanh(20.0*(Lnew - Lsc0)**2.0)
	coef = (1.0 + np.exp((TLsc - t_a)/tauD))

	xp = min(8.0, max(0.0, t_a/tauR))
	Frise = 0.02*(xp**3.0)*(8.0 - xp)**2.0*np.exp(-xp);
	
	Cnew = C + dt/tauR*CLsc*Frise + dt/tauD*(Crest/coef)
	Cnew = Cnew/(1.0 + dt/(tauD*coef))

	#Tact = Tmax * Frise * np.ones(len(Cnew));#Cnew * (Lnew - Lsc0) * (Ls - Lnew) / Lseiso
	Tact = Tmax * Cnew * (Lnew - Lsc0) * (Ls - Lnew) / Lseiso
	#Tact = Tact_temp*(Tact_temp > 0.0) + 0.0*(Tact_temp <= 0.0)

	return Cnew, Lnew, Tact
	
	
	






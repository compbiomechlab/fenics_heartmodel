import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import ode
from updateArtsVariable import *

def Arts(t, y, param):


	np.seterr(over="ignore")
	bhill = 1.5
	ahill = 1.5
	Crest = 0.04#0.0
	Ls0 = 1.51
	vmax = 7.0e-3#5.0e-3#
	tau_d = 32.0#33.8#
	tau_r = 48.0#28.1#
	tau_sc = 425.0#293.0#

	lbda, Lsref, Lseiso  = param
	Ls = lbda*Lsref

	Lsc, C = y.reshape(2, len(lbda))

	Lsnorm = (Ls - Lsc)/Lseiso
	x = min(8.0, max(0, t/tau_r))
	frise = 0.02*((8.0 - x)**2)*(x**3)*np.exp(-x)

	CL = np.tanh(20.0*(Lsc - Ls0)**2.0)*(Lsc > Ls0)
	T = tau_sc*(0.29 + 0.3*Lsc)


	#dy1 = ((Ls - Lsc)/Lseiso  - 1.0)*vmax
	dy1 = (Lsnorm - 1.0)/(bhill*Lsnorm + 1.0)*vmax*(Lsnorm <= 1) + (Lsnorm - 1.0)/(bhill*Lsnorm + 1.0)*vmax*np.exp(ahill*(Lsnorm-1.0))*(Lsnorm > 1)

	try:
    		expT = np.exp((T - t)/tau_d)
	except RuntimeWarning:
    		expT= float('inf')
	dy2 = 1.0/tau_r*CL*frise + 1/tau_d*(Crest - C)/(1 + expT)

	dy =  np.hstack((dy1,dy2))

	return dy


def updateArtsVariable_v2(Tmax, L, C, dt, lbda, t_a):

	Lsc0 = 1.51
	Lsref = 1.85
	Lseiso = 0.04
	arr_size = len(lbda)
	
	y0 = np.array([L, C])
	y0reshape = y0.reshape(1,2*arr_size)[0]
	param = [lbda, Lsref, Lseiso]
 
	backend = 'dopri5'
	ODEsolver = ode(Arts)
	ODEsolver.set_integrator(backend)
	ODEsolver.set_initial_value(y0reshape,t_a).set_f_params(param)

	y = ODEsolver.integrate(t_a + dt)

	Lsc = y.reshape(2,arr_size)[0]
	Csc = y.reshape(2,arr_size)[1]

	Tact = Tmax * Csc * (lbda*Lsref - Lsc)/Lseiso * (Lsc - Lsc0) * (Lsc > Lsc0)
	#Tact = Tmax * np.ones(arr_size)*math.sin(math.pi*t_a/400)*(t_a < 400)*0.5

	return Csc, Lsc, Tact


def readTact():

	fdata = open('Tact.txt', 'r')
	lines = fdata.readlines()

	lbda = []
	Tact = []
	for line in lines:
		lbda.append(float(line.rstrip().split()[2]))
		Tact.append(float(line.rstrip().split()[1]))

	return np.array(lbda), np.array(Tact)


def testArts():

	t0 = 0.0
	tF = 800
	num_steps = int(tF+1)
	tt = np.linspace(t0, tF, num_steps)
	
	#lbda_timearray, Tact_timearray = readTact()
	lbda_timearray = 1.4 - 0.85*np.sin(tt/tF*math.pi)

	arr_size = 1
	#lbda = np.array([0.6,0.8,0.9,1.0,1.1])
	lbda = np.array([lbda_timearray[0]])
	#Tact_fenics = np.array([Tact_timearray[0]])
	param = [lbda, 2.0, 0.04]
	dt = 1.0
	
	y0 = np.array([np.zeros(arr_size),np.zeros(arr_size)])
	y0reshape = y0.reshape(1,2*arr_size)[0]
	
	L = lbda_timearray[0]*1.85 - 0.04
	C = np.zeros(arr_size)
	
	Tact_array = np.zeros(arr_size)
	Lsc_array = np.ones(arr_size)*L 
	Tmax = 80e3
	
	t_a = 0.0
	time = [t0]
	dt = 1.0
	cnt = 0
	while (t_a < tF):
	
		print "t_a = ", t_a, " lbda = ", lbda , "Lref = ", lbda*1.85#, "Tact = ", Tact_fenics
		Csc, Lsc, Tact =  updateArtsVariable_v2(Tmax, L, C, dt, lbda, t_a)
		print "Csc = ", Csc, " Lsc = ", Lsc, " Tact = ", Tact
		t_a = t_a + dt

		try:
			lbda = np.array([lbda_timearray[cnt]])
			#Tact_fenics = Tact_timearray[cnt]
		except IndexError:
			lbda = np.array([lbda_timearray[len(lbda_timearray)-1]])
			#Tact_fenics = Tact_timearray[len(Tact_timearray)-1]
		cnt = cnt + 1
	
		L = Lsc
		C = Csc
		
		Tact_array = np.vstack([Tact_array,Tact])
		Lsc_array = np.vstack([Lsc_array,Lsc])
		time.append(t_a)
		
	
	plt.figure(1)
	plt.plot(time, Lsc_array[:,0], '-x')
	plt.plot(time, lbda_timearray*1.85, '-o')

	plt.figure(2)
	plt.plot(time, Tact_array, 'x')
	plt.show()

if __name__ == "__main__":
    testArts()

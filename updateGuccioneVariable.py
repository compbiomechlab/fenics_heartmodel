import numpy as np
import math as math

def updateGuccioneVariable(Tmax, L, C, dt, lbda, t_a):


	Ca0 = 4.35
	Ca0Max = 4.35
	B = 4.75
	L0 = 1.58
	#t0 = 100
	t0 = 150
	m = 1.0849e3
	b = -1.429e3
	Lsref = 1.85


	#lbda = 1.1*np.ones(len(lbda))
	Ls = lbda*Lsref	

	tr = m*Ls + b

	w = math.pi*t_a/t0*(t_a < t0) + math.pi*(t_a - t0 + tr)/tr*(t_a >= t0 and t_a < t0 + tr) + 0.0

	Ct = 0.5*(1.0 - np.cos(w))

	K = (np.exp(B*(Ls - L0)) - 1.0)*(Ls > L0) + 1.0e-12*(Ls <= L0)
	ECa50_den = np.sqrt(K)

	ECa50 = Ca0Max/ECa50_den

	Tact = Tmax * Ct * Ca0**2.0/(Ca0**2.0 + ECa50**2.0)
	Cnew = C
	Lnew = L

	return Cnew, Lnew, Tact
	
	
	






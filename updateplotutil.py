from matplotlib import pylab as plt

def plotPV(li, fighandle, LVcav_array, Pcav_array):

	li.set_xdata(LVcav_array)
	li.set_ydata(Pcav_array)
	plt.xlim([min(LVcav_array)-10,max(LVcav_array)+10])
	plt.ylim([min(Pcav_array)-10,max(Pcav_array)+10])
        fighandle.canvas.draw()





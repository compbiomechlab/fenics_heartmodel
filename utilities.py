from dolfin import *
import solver as solver

def cavityvol(w, X, n, ds, endoid, num_field):

    if(num_field > 1):
    	vol_form = -Constant(1.0/3.0) * inner(n,X+w.split()[0])*ds(endoid)
    else:
    	vol_form = -Constant(1.0/3.0) * inner(n,X+w)*ds(endoid)
    vol = assemble(vol_form)

    return vol

def wallvol(w,X):

    x = X + w.split()[0]
    F = grad(x)
    J = det(F)

    meshvol = assemble(J * dx)

    return meshvol

def cavitypressure(W, w):

    pnum = W.num_sub_spaces() - 1
    dofmap = W.sub(pnum).dofmap()
    val_dof = dofmap.cell_dofs(0)[0]
    pressure = -w.vector()[val_dof][0]

    return pressure

def Qtransform(f0, s0, n0):

	Q = as_matrix([[f0[0], f0[1], f0[2]], [s0[0], s0[1], s0[2]], [n0[0], n0[1], n0[2]]]);

	return Q

def computeNewVolume(p_cav, v_cav, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W):

	Press.P = p_cav
	V0.vol = v_cav

	#solver_param = {"newton_solver": {'maximum_iterations':20, 'absolute_tolerance':1e-5, 'relative_tolerance':1e-5}}
	solver_param = {'snes_solver': {'linear_solver': 'lu', 'maximum_iterations': 30, 'relative_tolerance': 1e-05, 'error_on_nonconvergence': True, 'report': True, 'absolute_tolerance': 1e-05, 'method': 'newtonls', 'line_search': 'basic'}, 'nonlinear_solver': 'snes'}


	solve(F == 0, w, bcs, J=Jac, solver_parameters=solver_param, form_compiler_parameters=ffc_options)

	if(isvolctrl):
		V_cav = cavityvol(w.split()[0], X, n, ds, endoid, num_field)
		P_cav = -1.0*cavitypressure(W,w)
	else:
		V_cav = cavityvol(w.split()[0], X, n, ds, endoid, num_field)
		P_cav = p_cav


	return V_cav, P_cav


def computeNewPressure(V_cav, P_cav0, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W):

	pressure_tol = 1.0e0;
	pdiff = 100;
	its = 0
  	max_its = 10;
	Vtemp = 0;
	ptemp = 0;

	pressure = P_cav0

	print "Entering Newton loop to calculate new cavity pressure"
	while(abs(pdiff) > pressure_tol and its <= max_its):
		
		print "Computing compliance"
		Ch = computeCompliance(pressure, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W)		

		print "Computing new volume"
		V_fem, ptemp = computeNewVolume(pressure, Vtemp, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W)

		Vdiff = V_cav - V_fem

		pdiff = Vdiff / Ch

		pressure = pressure + pdiff
		print "pdiff = ", pdiff, "pressure ",  pressure

	print "Exiting Newton loop to calculate new cavity pressure"

	return pressure, w


def computeCompliance(p_cav, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W):
	
	Vtemp = 0;
	V_cav, ptemp = computeNewVolume(p_cav, Vtemp, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W)
	delta = 1e0;
	
	p_diff = p_cav + delta
	V_cav2, ptemp = computeNewVolume(p_diff, Vtemp, w, bcs, F, Jac, ffc_options, num_field, Press, X, ds, endoid, n, isvolctrl, V0, W)

	
	comp = (V_cav2 - V_cav)/delta

	return comp


